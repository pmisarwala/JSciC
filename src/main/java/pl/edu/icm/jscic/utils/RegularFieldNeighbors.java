/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.utils;

/**
 * Static methods that return regular filed neighbors.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegularFieldNeighbors {

    private static final float INV_SQRT2 = (float) (1 / Math.sqrt(2));
    private static final float INV_SQRT3 = (float) (1 / Math.sqrt(3));

    /**
     * Weighted neighbors in 3D.
     */
    public static final float[][] WEIGHTS3D
            = new float[][]{{INV_SQRT3, INV_SQRT2, INV_SQRT3, INV_SQRT2, 1, INV_SQRT2, INV_SQRT3, INV_SQRT2, INV_SQRT3,
                INV_SQRT2, 1, INV_SQRT2, 1, 1, INV_SQRT2, 1, INV_SQRT2,
                INV_SQRT3, INV_SQRT2, INV_SQRT3, INV_SQRT2, 1, INV_SQRT2, INV_SQRT3, INV_SQRT2, INV_SQRT3},
            {INV_SQRT2, INV_SQRT2, 1, INV_SQRT2, INV_SQRT2,
                INV_SQRT2, 1, INV_SQRT2, 1, 1, INV_SQRT2, 1, INV_SQRT2,
                INV_SQRT2, INV_SQRT2, 1, INV_SQRT2, INV_SQRT2},
            {1, 1, 1, 1, 1, 1}};

    /**
     * Weighted neighbors in 2D.
     */
    public static final float[][] WEIGHTS2D
            = new float[][]{{INV_SQRT2, 1, INV_SQRT2, 1, 1, INV_SQRT2, 1, INV_SQRT2},
            {1, 1, 1, 1},
            {1, 1, 1, 1}};
    /**
     * Weighted neighbors in 1D.
     */
    public static final float[][] WEIGHTS1D
            = new float[][]{{1, 1}, {1, 1}, {1, 1}};
    /**
     * Weighted neighbors in 1D, 2D, and 3D.
     */
    public static final float[][][] WEIGHTS = new float[][][]{null, WEIGHTS1D, WEIGHTS2D, WEIGHTS3D};

    /**
     * Symmetric weighted neighbors in 3D.
     */
    public static final float[][] SYMMETRIC_WEIGHTS3D
            = new float[][]{{1, INV_SQRT2, 1, INV_SQRT2,
                INV_SQRT3, INV_SQRT2, INV_SQRT3, INV_SQRT2, 1, INV_SQRT2, INV_SQRT3, INV_SQRT2, INV_SQRT3},
            {1, INV_SQRT2, 1, INV_SQRT2,
                INV_SQRT2, INV_SQRT2, 1, INV_SQRT2, INV_SQRT2},
            {1, 1, 1}};

    /**
     * Symmetric weighted neighbors in 2D
     */
    public static final float[][] SYMMETRIC_WEIGHTS2D
            = new float[][]{{1, INV_SQRT2, 1, INV_SQRT2},
            {1, 1},
            {1, 1}};

    /**
     * Symmetric weighted neighbors in 1D
     */
    public static final float[][] SYMMETRIC_WEIGHTS1D
            = new float[][]{{1}, {1}, {1}};

    /**
     * Symmetric weighted neighbors in 1D, 2D, and 3D
     */
    public static final float[][][] SYMMETRIC_WEIGHTS = new float[][][]{null, SYMMETRIC_WEIGHTS1D, SYMMETRIC_WEIGHTS2D, SYMMETRIC_WEIGHTS3D};

    /**
     * Returns neighbor indices for the give dimensions
     *
     * @param dims regular filed dimensions
     * @return neighbor indices
     */
    public static int[][] neighbors(int[] dims) {
        switch (dims.length) {
            case 3:
                return new int[][]{
                    {-dims[1] * dims[0] - dims[0] - 1, -dims[1] * dims[0] - dims[0], -dims[1] * dims[0] - dims[0] + 1,
                        -dims[1] * dims[0] - 1, -dims[1] * dims[0], -dims[1] * dims[0] + 1,
                        -dims[1] * dims[0] + dims[0] - 1, -dims[1] * dims[0] + dims[0], -dims[1] * dims[0] + dims[0] + 1,
                        -dims[0] - 1, -dims[0], -dims[0] + 1,
                        - 1, 1,
                        +dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0] - 1, dims[1] * dims[0] - dims[0], dims[1] * dims[0] - dims[0] + 1,
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0] - 1, dims[1] * dims[0] + dims[0], dims[1] * dims[0] + dims[0] + 1
                    },
                    {-dims[1] * dims[0] - dims[0],
                        -dims[1] * dims[0] - 1, -dims[1] * dims[0], -dims[1] * dims[0] + 1,
                        -dims[1] * dims[0] + dims[0],
                        -dims[0] - 1, -dims[0], -dims[0] + 1,
                        - 1, 1,
                        +dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0],
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0],},
                    {-dims[1] * dims[0], -dims[0], -1, 1, dims[0], dims[1] * dims[0]
                    }
                };
            case 2:
               return new int[][]{
                    {
                        -dims[0] - 1, -dims[0], -dims[0] + 1,
                        - 1, 1,
                        dims[0] - 1, dims[0], dims[0] + 1
                    },
                    {-dims[0], - 1, 1, dims[0]},
                    {-dims[0], - 1, 1, dims[0]}
                };
            case 1:
                return new int[][]{
                    {- 1, 1},
                    {- 1, 1},
                    {- 1, 1}
                };
        }
        return null;
    }

    /**
     * Returns neighbor indices for the give dimensions
     *
     * @param dims regular filed dimensions
     * @return neighbor indices
     */
    public static long[][] neighbors(long[] dims) {
        switch (dims.length) {
            case 3:
                return new long[][]{
                    {-dims[1] * dims[0] - dims[0] - 1, -dims[1] * dims[0] - dims[0], -dims[1] * dims[0] - dims[0] + 1,
                        -dims[1] * dims[0] - 1, -dims[1] * dims[0], -dims[1] * dims[0] + 1,
                        -dims[1] * dims[0] + dims[0] - 1, -dims[1] * dims[0] + dims[0], -dims[1] * dims[0] + dims[0] + 1,
                        -dims[0] - 1, -dims[0], -dims[0] + 1,
                        - 1, 1,
                        +dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0] - 1, dims[1] * dims[0] - dims[0], dims[1] * dims[0] - dims[0] + 1,
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0] - 1, dims[1] * dims[0] + dims[0], dims[1] * dims[0] + dims[0] + 1
                    },
                    {-dims[1] * dims[0] - dims[0],
                        -dims[1] * dims[0] - 1, -dims[1] * dims[0], -dims[1] * dims[0] + 1,
                        -dims[1] * dims[0] + dims[0],
                        -dims[0] - 1, -dims[0], -dims[0] + 1,
                        - 1, 1,
                        +dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0],
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0],},
                    {-dims[1] * dims[0], -dims[0], -1, 1, dims[0], dims[1] * dims[0]
                    }
                };
            case 2:
                return new long[][]{
                    {
                        -dims[0] - 1, -dims[0], -dims[0] + 1,
                        - 1, 1,
                        dims[0] - 1, dims[0], dims[0] + 1
                    },
                    {-dims[0], - 1, 1, dims[0]},
                    {-dims[0], - 1, 1, dims[0]}
                };
            case 1:
                return new long[][]{
                    {- 1, 1},
                    {- 1, 1},
                    {- 1, 1}
                };
        }
        return null;
    }

    /**
     * Returns symmetric neighbor indices for the give dimensions
     * @param dims regular filed dimensions
     * @return symmtric neighbor indices 
     */
    public static int[][] symmetricNeighbors(int[] dims) {
        switch (dims.length) {
            case 3:
                return new int[][]{
                    {+dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0] - 1, dims[1] * dims[0] - dims[0], dims[1] * dims[0] - dims[0] + 1,
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0] - 1, dims[1] * dims[0] + dims[0], dims[1] * dims[0] + dims[0] + 1
                    },
                    {1,
                        +dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0],
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0],},
                    {1, dims[0], dims[1] * dims[0]}
                };
            case 2:
                return new int[][]{
                    {1,
                        dims[0] - 1, dims[0], dims[0] + 1
                    },
                    {1, dims[0]},
                    {1, dims[0]}
                };
            case 1:
                return new int[][]{
                    {1},
                    {1},
                    {1}
                };
        }
        return null;
    }

    /**
     * Returns symmetric neighbor indices for the give dimensions
     * @param dims regular filed dimensions
     * @return symmtric neighbor indices 
     */
    public static long[][] symmetricNeighbors(long[] dims) {
        switch (dims.length) {
            case 3:
                return new long[][]{
                    {+dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0] - 1, dims[1] * dims[0] - dims[0], dims[1] * dims[0] - dims[0] + 1,
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0] - 1, dims[1] * dims[0] + dims[0], dims[1] * dims[0] + dims[0] + 1
                    },
                    {1,
                        +dims[0] - 1, +dims[0], +dims[0] + 1,
                        dims[1] * dims[0] - dims[0],
                        dims[1] * dims[0] - 1, dims[1] * dims[0], dims[1] * dims[0] + 1,
                        dims[1] * dims[0] + dims[0],},
                    {1, dims[0], dims[1] * dims[0]}
                };
            case 2:
                return new long[][]{
                    {1,
                        dims[0] - 1, dims[0], dims[0] + 1
                    },
                    {1, dims[0]},
                    {1, dims[0]}
                };
            case 1:
                return new long[][]{
                    {1},
                    {1},
                    {1}
                };
        }
        return null;
    }

    private RegularFieldNeighbors() {
    }

}
