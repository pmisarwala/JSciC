/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.utils;

import static org.apache.commons.math3.util.FastMath.*;

/**
 * Various numerical operations on scalar values.
 * 
 * @author Piotr Wendykier (piotrw@icm.edu.pl), University of Warsaw, ICM
 *
 */
public class ScalarMath
{

    /**
     * Computers a power-of-two for a given exponent.
     * 
     * @param exponent exponent
     *                 
     * @return 2^exponent
     */
    public static int power2(int exponent)
    {
        if (exponent < 0 || exponent > 30) throw new IllegalArgumentException("exponent must be non-negative and smaller than 31");
        return 1 << exponent;
    }

    /**
     * Computers a power-of-two for a given exponent.
     * 
     * @param exponent exponent
     *                 
     * @return 2^exponent
     */
    public static long power2(long exponent)
    {
        if (exponent < 0 || exponent > 62) throw new IllegalArgumentException("exponent must be non-negative and smaller than 63");
        return 1l << exponent;
    }

    /**
     * Checks if x is a power-of-two number.
     * 
     * @param x input number
     *          
     * @return true if x is power of 2
     */
    public static boolean isPower2(int x)
    {
        if (x <= 0)
            return false;
        else
            return (x & (x - 1)) == 0;
    }

    /**
     * Checks if x is a power-of-two number.
     * 
     * @param x input number
     *          
     * @return true if x is power of 2
     */
    public static boolean isPower2(long x)
    {
        if (x <= 0)
            return false;
        else
            return (x & (x - 1l)) == 0;
    }

    /**
     * Returns the closest power of two less than or equal to x.
     * 
     * @param x input number
     *          
     * @return the closest power of two less then or equal to x
     */
    public static int previousPower2(int x)
    {
        if (x < 1)
            throw new IllegalArgumentException("x must be positive");
        return power2((int) floor(log(x) / log(2)));
    }

    /**
     * Returns the closest power of two less than or equal to x.
     * 
     * @param x input number
     *          
     * @return the closest power of two less then or equal to x
     */
    public static long previousPower2(long x)
    {
        if (x < 1)
            throw new IllegalArgumentException("x must be positive");
        return power2((long) floor(log(x) / log(2)));
    }

    /**
     * Returns the closest power of two greater than or equal to x.
     * 
     * @param x input number
     *          
     * @return the closest power of two greater than or equal to x
     */
    public static int nextPower2(int x)
    {
        if (x < 1)
            throw new IllegalArgumentException("x must be positive");
        if ((x & (x - 1)) == 0) {
            return x; // x is already a power-of-two number 
        }
        return Integer.highestOneBit(x);
    }

    /**
     * Returns the closest power of two greater than or equal to x.
     * 
     * @param x input number
     *          
     * @return the closest power of two greater than or equal to x
     */
    public static long nextPower2(long x)
    {
        if (x < 1)
            throw new IllegalArgumentException("x must be positive");
        if ((x & (x - 1l)) == 0) {
            return x; // x is already a power-of-two number 
        }
        return Long.highestOneBit(x);
    }
    
    /**
     * Computes linear mapping coefficients between given ranges.
     * @param start1 start of the first range
     * @param end1 end of the first range
     * @param start2 start of the second range
     * @param end2 end of the second range
     * @return linear mapping coefficients
     */
    public static double[] linearMappingCoefficients(double start1, double end1, double start2, double end2) {
        if(end1 < start1 || end2 < start2) throw new IllegalArgumentException("end1 < start1 || end2 < start2");
        double[] res = new double[2];
        double len1 = end1 - start1;
        double len2 = end2 - start2;
        res[0] = len2 / len1;
        res[1] = start2 - start1*res[0];
        return res;
    }
}
