/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.utils;

/**
 * Physical constants.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public enum PhysicalConstants
{
    ATOMIC_MASS
    {
        @Override
        public String getDescription()
        {
            return "atomic mass constant";
        }

        @Override
        public String getSymbol()
        {
            return "m_u";
        }

        @Override
        public double getValue()
        {
            return 1.660539040E-27;
        }

        @Override
        public String getUnit()
        {
            return "kg";
        }
    },
    AVOGARDO_NA
    {
        @Override
        public String getDescription()
        {
            return "Avogadro constant (N_A)";
        }

        @Override
        public String getSymbol()
        {
            return "N_A";
        }

        @Override
        public double getValue()
        {
            return 6.022140857E23;
        }

        @Override
        public String getUnit()
        {
            return "mol-1";
        }
    }
    ,
    AVOGARDO_L
    {
        @Override
        public String getDescription()
        {
            return "Avogadro constant (L)";
        }

        @Override
        public String getSymbol()
        {
            return "L";
        }

        @Override
        public double getValue()
        {
            return 6.022140857E23;
        }

        @Override
        public String getUnit()
        {
            return "mol-1";
        }
    },
    BOLTZMANN
    {
        @Override
        public String getDescription()
        {
            return "Boltzmann constant";
        }

        @Override
        public String getSymbol()
        {
            return "k";
        }

        @Override
        public double getValue()
        {
            return 1.38064852E-23;
        }

        @Override
        public String getUnit()
        {
            return "J K-1";
        }
    },
    CONDUCTANCE_QUANTUM
    {
        @Override
        public String getDescription()
        {
            return "conductance quantum";
        }

        @Override
        public String getSymbol()
        {
            return "G_0";
        }

        @Override
        public double getValue()
        {
            return 7.7480917310E-5;
        }

        @Override
        public String getUnit()
        {
            return "S";
        }
    },
    ELECTRIC
    {
        @Override
        public String getDescription()
        {
            return "electric constant";
        }

        @Override
        public String getSymbol()
        {
            return "epsilon_0";
        }

        @Override
        public double getValue()
        {
            return 8.854187817E-12;
        }

        @Override
        public String getUnit()
        {
            return "F m-1";
        }
    },
    ELECTRON_MASS
    {
        @Override
        public String getDescription()
        {
            return "electron mass";
        }

        @Override
        public String getSymbol()
        {
            return "m_e";
        }

        @Override
        public double getValue()
        {
            return 9.10938356E-31;
        }

        @Override
        public String getUnit()
        {
            return "kg";
        }
    },
    ELECTRON_VOLT
    {
        @Override
        public String getDescription()
        {
            return "electron volt";
        }

        @Override
        public String getSymbol()
        {
            return "eV";
        }

        @Override
        public double getValue()
        {
            return 1.6021766208E-19;
        }

        @Override
        public String getUnit()
        {
            return "J";
        }
    },
    ELEMENTARY_CHARGE
    {
        @Override
        public String getDescription()
        {
            return "elementary charge";
        }

        @Override
        public String getSymbol()
        {
            return "e";
        }

        @Override
        public double getValue()
        {
            return 1.6021766208E-19;
        }

        @Override
        public String getUnit()
        {
            return "C";
        }
    },
    FARADAY
    {
        @Override
        public String getDescription()
        {
            return "Faraday constant";
        }

        @Override
        public String getSymbol()
        {
            return "f";
        }

        @Override
        public double getValue()
        {
            return 96485.33289;
        }

        @Override
        public String getUnit()
        {
            return "C mol-1";
        }
    },
    FINE_STRUCTURE
    {
        @Override
        public String getDescription()
        {
            return "fine-structure constant";
        }

        @Override
        public String getSymbol()
        {
            return "alpha";
        }

        @Override
        public double getValue()
        {
            return 7.2973525664E-3;
        }

        @Override
        public String getUnit()
        {
            return "1";
        }
    },
    MAGNETIC
    {
        @Override
        public String getDescription()
        {
            return "magnetic constant";
        }

        @Override
        public String getSymbol()
        {
            return "mu_0";
        }

        @Override
        public double getValue()
        {
            return 12.566370614E-7;
        }

        @Override
        public String getUnit()
        {
            return "N A-2";
        }
    },
    MAGNETIC_FLUX_QUANTUM
    {
        @Override
        public String getDescription()
        {
            return "magnetic flux quantum";
        }

        @Override
        public String getSymbol()
        {
            return "Phi_0";
        }

        @Override
        public double getValue()
        {
            return 2.067833831E-15;
        }

        @Override
        public String getUnit()
        {
            return "Wb";
        }
    },
    MOLAR_GAS
    {
        @Override
        public String getDescription()
        {
            return "molar gas constant";
        }

        @Override
        public String getSymbol()
        {
            return "R";
        }

        @Override
        public double getValue()
        {
            return 8.3144598;
        }

        @Override
        public String getUnit()
        {
            return "J mol-1 K-1";
        }
    },
    NEWTONIAN_CONSTANT_OF_GRAVITATION
    {
        @Override
        public String getDescription()
        {
            return "Newtonian constant of gravitation";
        }

        @Override
        public String getSymbol()
        {
            return "G";
        }

        @Override
        public double getValue()
        {
            return 6.67408E-11;
        }

        @Override
        public String getUnit()
        {
            return "m3 kg-1 s-2";
        }
    },
    PLANCK
    {
        @Override
        public String getDescription()
        {
            return "Planck constant";
        }

        @Override
        public String getSymbol()
        {
            return "h";
        }

        @Override
        public double getValue()
        {
            return 6.626070040E-34;
        }

        @Override
        public String getUnit()
        {
            return "J s";
        }
    },
    PROTON_MASS
    {
        @Override
        public String getDescription()
        {
            return "proton mass";
        }

        @Override
        public String getSymbol()
        {
            return "m_p";
        }

        @Override
        public double getValue()
        {
            return 1.672621898E-27;
        }

        @Override
        public String getUnit()
        {
            return "kg";
        }
    },
    RYDBERG
    {
        @Override
        public String getDescription()
        {
            return "Rydberg constant";
        }

        @Override
        public String getSymbol()
        {
            return "R_inf";
        }

        @Override
        public double getValue()
        {
            return 10973731.568508;
        }

        @Override
        public String getUnit()
        {
            return "m-1";
        }
    },
    SPEED_OF_LIGHT
    {
        @Override
        public String getDescription()
        {
            return "speed of light in vacuum";
        }

        @Override
        public String getSymbol()
        {
            return "c";
        }

        @Override
        public double getValue()
        {
            return 299792458;
        }

        @Override
        public String getUnit()
        {
            return "m s-1";
        }
    },
    STEFAN_BOLTZMAN
    {
        @Override
        public String getDescription()
        {
            return "Stefan-Boltzmann constant";
        }

        @Override
        public String getSymbol()
        {
            return "sigma";
        }

        @Override
        public double getValue()
        {
            return 5.670367E-8;
        }

        @Override
        public String getUnit()
        {
            return "W m-2 K-4";
        }
    };

    /**
     * Returns the description of a physical constant.
     *
     * @return the description of a physical constant
     */
    public String getDescription()
    {
        return "";
    }

    /**
     * Returns the symbol of a physical constant.
     *
     * @return the symbol of a physical constant
     */
    public String getSymbol()
    {
        return "";
    }

    /**
     * Returns the value of a physical constant.
     *
     * @return the value of a physical constant
     */
    public double getValue()
    {
        return 0;
    }

    /**
     * Returns the unit of a physical constant.
     *
     * @return the unit of a physical constant
     */
    public String getUnit()
    {
        return "1";
    }

}
