/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.utils;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.commons.math3.util.FastMath;
import pl.edu.icm.jlargearrays.LargeArrayArithmetics;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;

/**
 * Arithmetical operations on DataArrays.
 * 
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class DataArrayArithmetics
{

    /**
     * Addition of two DataArrays (da1 + da2) computed in a single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar + scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar + vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector + scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>addition of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 + da2
     */
    public static DataArray addF(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.add(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setFloat(i * veclen2 + v, a.getFloat(i) + b.getFloat(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setFloat(k * veclen2 + v, a.getFloat(k) + b.getFloat(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setFloat(i * veclen2 + v, a.getFloat(i) + b.getFloat(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        float[] elem_res = new float[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                float[] elem_a = ac.getComplexFloat(i);
                                float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                elem_res[0] = elem_a[0] + elem_b[0];
                                elem_res[1] = elem_a[1] + elem_b[1];
                                resc.setComplexFloat(i * veclen2 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    float[] elem_res = new float[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            float[] elem_a = ac.getComplexFloat(k);
                                            float[] elem_b = bc.getComplexFloat(k * veclen2 + v);
                                            elem_res[0] = elem_a[0] + elem_b[0];
                                            elem_res[1] = elem_a[1] + elem_b[1];
                                            resc.setComplexFloat(k * veclen2 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            float[] elem_res = new float[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    float[] elem_a = ac.getComplexFloat(i);
                                    float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                    elem_res[0] = elem_a[0] + elem_b[0];
                                    elem_res[1] = elem_a[1] + elem_b[1];
                                    resc.setComplexFloat(i * veclen2 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) + b.getFloat(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setFloat(k * veclen1 + v, a.getFloat(k * veclen1 + v) + b.getFloat(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) + b.getFloat(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        float[] elem_res = new float[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                float[] elem_b = bc.getComplexFloat(i);
                                elem_res[0] = elem_a[0] + elem_b[0];
                                elem_res[1] = elem_a[1] + elem_b[1];
                                resc.setComplexFloat(i * veclen1 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    float[] elem_res = new float[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen1 + v);
                                            float[] elem_b = bc.getComplexFloat(k);
                                            elem_res[0] = elem_a[0] + elem_b[0];
                                            elem_res[1] = elem_a[1] + elem_b[1];
                                            resc.setComplexFloat(k * veclen1 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            float[] elem_res = new float[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                    float[] elem_b = bc.getComplexFloat(i);
                                    elem_res[0] = elem_a[0] + elem_b[0];
                                    elem_res[1] = elem_a[1] + elem_b[1];
                                    resc.setComplexFloat(i * veclen1 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_addF_" + da2.getName());
    }

    /**
     * Addition of two DataArrays (da1 + da2) computed in a double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar + scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar + vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector + scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>addition of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 + da2
     */
    public static DataArray addD(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.add(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setDouble(i * veclen2 + v, a.getDouble(i) + b.getDouble(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setDouble(k * veclen2 + v, a.getDouble(k) + b.getDouble(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setDouble(i * veclen2 + v, a.getDouble(i) + b.getDouble(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        double[] elem_res = new double[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                double[] elem_a = ac.getComplexDouble(i);
                                double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                elem_res[0] = elem_a[0] + elem_b[0];
                                elem_res[1] = elem_a[1] + elem_b[1];
                                resc.setComplexDouble(i * veclen2 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    double[] elem_res = new double[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            double[] elem_a = ac.getComplexDouble(k);
                                            double[] elem_b = bc.getComplexDouble(k * veclen2 + v);
                                            elem_res[0] = elem_a[0] + elem_b[0];
                                            elem_res[1] = elem_a[1] + elem_b[1];
                                            resc.setComplexDouble(k * veclen2 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            double[] elem_res = new double[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    double[] elem_a = ac.getComplexDouble(i);
                                    double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                    elem_res[0] = elem_a[0] + elem_b[0];
                                    elem_res[1] = elem_a[1] + elem_b[1];
                                    resc.setComplexDouble(i * veclen2 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) + b.getDouble(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setDouble(k * veclen1 + v, a.getDouble(k * veclen1 + v) + b.getDouble(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) + b.getDouble(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        double[] elem_res = new double[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                double[] elem_b = bc.getComplexDouble(i);
                                elem_res[0] = elem_a[0] + elem_b[0];
                                elem_res[1] = elem_a[1] + elem_b[1];
                                resc.setComplexDouble(i * veclen1 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    double[] elem_res = new double[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            double[] elem_a = ac.getComplexDouble(k * veclen1 + v);
                                            double[] elem_b = bc.getComplexDouble(k);
                                            elem_res[0] = elem_a[0] + elem_b[0];
                                            elem_res[1] = elem_a[1] + elem_b[1];
                                            resc.setComplexDouble(k * veclen1 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            double[] elem_res = new double[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                    double[] elem_b = bc.getComplexDouble(i);
                                    elem_res[0] = elem_a[0] + elem_b[0];
                                    elem_res[1] = elem_a[1] + elem_b[1];
                                    resc.setComplexDouble(i * veclen1 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_addD_" + da2.getName());
    }

    /**
     * Subtraction of two DataArrays (da1 - da2) computed in a single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar - scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar - vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector - scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>subtraction of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 - da2
     */
    public static DataArray diffF(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.diff(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setFloat(i * veclen2 + v, a.getFloat(i) - b.getFloat(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setFloat(k * veclen2 + v, a.getFloat(k) - b.getFloat(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setFloat(i * veclen2 + v, a.getFloat(i) - b.getFloat(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        float[] elem_res = new float[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                float[] elem_a = ac.getComplexFloat(i);
                                float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                elem_res[0] = elem_a[0] - elem_b[0];
                                elem_res[1] = elem_a[1] - elem_b[1];
                                resc.setComplexFloat(i * veclen2 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    float[] elem_res = new float[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            float[] elem_a = ac.getComplexFloat(k);
                                            float[] elem_b = bc.getComplexFloat(k * veclen2 + v);
                                            elem_res[0] = elem_a[0] - elem_b[0];
                                            elem_res[1] = elem_a[1] - elem_b[1];
                                            resc.setComplexFloat(k * veclen2 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            float[] elem_res = new float[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    float[] elem_a = ac.getComplexFloat(i);
                                    float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                    elem_res[0] = elem_a[0] - elem_b[0];
                                    elem_res[1] = elem_a[1] - elem_b[1];
                                    resc.setComplexFloat(i * veclen2 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) - b.getFloat(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setFloat(k * veclen1 + v, a.getFloat(k * veclen1 + v) - b.getFloat(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) - b.getFloat(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        float[] elem_res = new float[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                float[] elem_b = bc.getComplexFloat(i);
                                elem_res[0] = elem_a[0] - elem_b[0];
                                elem_res[1] = elem_a[1] - elem_b[1];
                                resc.setComplexFloat(i * veclen1 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    float[] elem_res = new float[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen1 + v);
                                            float[] elem_b = bc.getComplexFloat(k);
                                            elem_res[0] = elem_a[0] - elem_b[0];
                                            elem_res[1] = elem_a[1] - elem_b[1];
                                            resc.setComplexFloat(k * veclen1 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            float[] elem_res = new float[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                    float[] elem_b = bc.getComplexFloat(i);
                                    elem_res[0] = elem_a[0] - elem_b[0];
                                    elem_res[1] = elem_a[1] - elem_b[1];
                                    resc.setComplexFloat(i * veclen1 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_diffF_" + da2.getName());
    }

    /**
     * Subtraction of two DataArrays (da1 - da2) computed in a double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar - scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar - vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector - scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>subtraction of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 - da2
     */
    public static DataArray diffD(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.diff(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setDouble(i * veclen2 + v, a.getDouble(i) - b.getDouble(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setDouble(k * veclen2 + v, a.getDouble(k) - b.getDouble(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setDouble(i * veclen2 + v, a.getDouble(i) - b.getDouble(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        double[] elem_res = new double[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                double[] elem_a = ac.getComplexDouble(i);
                                double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                elem_res[0] = elem_a[0] - elem_b[0];
                                elem_res[1] = elem_a[1] - elem_b[1];
                                resc.setComplexDouble(i * veclen2 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    double[] elem_res = new double[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            double[] elem_a = ac.getComplexDouble(k);
                                            double[] elem_b = bc.getComplexDouble(k * veclen2 + v);
                                            elem_res[0] = elem_a[0] - elem_b[0];
                                            elem_res[1] = elem_a[1] - elem_b[1];
                                            resc.setComplexDouble(k * veclen2 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            double[] elem_res = new double[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    double[] elem_a = ac.getComplexDouble(i);
                                    double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                    elem_res[0] = elem_a[0] - elem_b[0];
                                    elem_res[1] = elem_a[1] - elem_b[1];
                                    resc.setComplexDouble(i * veclen2 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) - b.getDouble(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setDouble(k * veclen1 + v, a.getDouble(k * veclen1 + v) - b.getDouble(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) - b.getDouble(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        double[] elem_res = new double[2];
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                double[] elem_b = bc.getComplexDouble(i);
                                elem_res[0] = elem_a[0] - elem_b[0];
                                elem_res[1] = elem_a[1] - elem_b[1];
                                resc.setComplexDouble(i * veclen1 + v, elem_res);
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    double[] elem_res = new double[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            double[] elem_a = ac.getComplexDouble(k * veclen1 + v);
                                            double[] elem_b = bc.getComplexDouble(k);
                                            elem_res[0] = elem_a[0] - elem_b[0];
                                            elem_res[1] = elem_a[1] - elem_b[1];
                                            resc.setComplexDouble(k * veclen1 + v, elem_res);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            double[] elem_res = new double[2];
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                    double[] elem_b = bc.getComplexDouble(i);
                                    elem_res[0] = elem_a[0] - elem_b[0];
                                    elem_res[1] = elem_a[1] - elem_b[1];
                                    resc.setComplexDouble(i * veclen1 + v, elem_res);
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_diffD_" + da2.getName());
    }

    /**
     * Multiplication of two DataArrays (da1 * da2) computed in a single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar * scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar * vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector * scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>dot product of two vectors of equal length</td><td>scalar</td>
     * </tr>
     * </table>
     * 
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 * da2
     */
    public static DataArray multF(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();
        int veclen = FastMath.max(veclen1, veclen2);

        if (veclen1 == 1 && veclen2 == 1) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.mult(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setFloat(i * veclen2 + v, a.getFloat(i) * b.getFloat(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setFloat(k * veclen2 + v, a.getFloat(k) * b.getFloat(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setFloat(i * veclen2 + v, a.getFloat(i) * b.getFloat(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                float[] elem_a = ac.getComplexFloat(i);
                                float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                resc.setComplexFloat(i * veclen2 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            float[] elem_a = ac.getComplexFloat(k);
                                            float[] elem_b = bc.getComplexFloat(k * veclen2 + v);

                                            resc.setComplexFloat(k * veclen2 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    float[] elem_a = ac.getComplexFloat(i);
                                    float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                    resc.setComplexFloat(i * veclen2 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) * b.getFloat(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setFloat(k * veclen1 + v, a.getFloat(k * veclen1 + v) * b.getFloat(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) * b.getFloat(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                float[] elem_b = bc.getComplexFloat(i);
                                resc.setComplexFloat(i * veclen1 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen1 + v);
                                            float[] elem_b = bc.getComplexFloat(k);
                                            resc.setComplexFloat(k * veclen1 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                    float[] elem_b = bc.getComplexFloat(i);
                                    resc.setComplexFloat(i * veclen1 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen1 == veclen2) { //dot product
            veclen = 1;
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length() / veclen1;
                final LargeArray res = LargeArrayUtils.create(out_type, length, false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            float dot = 0;
                            for (int v = 0; v < veclen1; v++) {
                                dot += a.getFloat(i * veclen1 + v) * b.getFloat(i * veclen1 + v);
                            }
                            res.setFloat(i, dot);
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        float dot = 0;
                                        for (int v = 0; v < veclen1; v++) {
                                            dot += a.getFloat(k * veclen1 + v) * b.getFloat(k * veclen1 + v);
                                        }
                                        res.setFloat(k, dot);
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                float dot = 0;
                                for (int v = 0; v < veclen1; v++) {
                                    dot += a.getFloat(i * veclen1 + v) * b.getFloat(i * veclen1 + v);
                                }
                                res.setFloat(i, dot);
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        float[] dot = new float[2];
                        for (long i = 0; i < length; i++) {
                            dot[0] = dot[1] = 0;
                            for (int v = 0; v < veclen1; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                float[] elem_b = bc.getComplexFloat(i * veclen1 + v);
                                dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                            }
                            resc.setComplexFloat(i, dot);
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    float[] dot = new float[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        dot[0] = dot[1] = 0;
                                        for (int v = 0; v < veclen1; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen1 + v);
                                            float[] elem_b = bc.getComplexFloat(k * veclen1 + v);
                                            dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                        }
                                        resc.setComplexFloat(k, dot);
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            float[] dot = new float[2];
                            for (long i = 0; i < length; i++) {
                                dot[0] = dot[1] = 0;
                                for (int v = 0; v < veclen1; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                    float[] elem_b = bc.getComplexFloat(i * veclen1 + v);
                                    dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                }
                                resc.setComplexFloat(i, dot);
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da1.getName() + "_multF_" + da2.getName());
    }

    /**
     * Multiplication of two DataArrays (da1 * da2) computed in a double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar * scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar * vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector * scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>dot product of two vectors of equal length</td><td>scalar</td>
     * </tr>
     * </table>
     * 
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 * da2
     */
    public static DataArray multD(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();
        int veclen = FastMath.max(veclen1, veclen2);

        if (veclen1 == 1 && veclen2 == 1) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.mult(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setDouble(i * veclen2 + v, a.getDouble(i) * b.getDouble(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setDouble(k * veclen2 + v, a.getDouble(k) * b.getDouble(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setDouble(i * veclen2 + v, a.getDouble(i) * b.getDouble(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                double[] elem_a = ac.getComplexDouble(i);
                                double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                resc.setComplexDouble(i * veclen2 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            double[] elem_a = ac.getComplexDouble(k);
                                            double[] elem_b = bc.getComplexDouble(k * veclen2 + v);
                                            resc.setComplexDouble(k * veclen2 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    double[] elem_a = ac.getComplexDouble(i);
                                    double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                    resc.setComplexDouble(i * veclen2 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) * b.getDouble(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setDouble(k * veclen1 + v, a.getDouble(k * veclen1 + v) * b.getDouble(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) * b.getDouble(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                double[] elem_b = bc.getComplexDouble(i);
                                resc.setComplexDouble(i * veclen1 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            double[] elem_a = ac.getComplexDouble(k * veclen1 + v);
                                            double[] elem_b = bc.getComplexDouble(k);
                                            resc.setComplexDouble(k * veclen1 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                    double[] elem_b = bc.getComplexDouble(i);
                                    resc.setComplexDouble(i * veclen1 + v, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen1 == veclen2) { //dot product
            veclen = 1;
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length() / veclen1;
                final LargeArray res = LargeArrayUtils.create(out_type, length, false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            double dot = 0;
                            for (int v = 0; v < veclen1; v++) {
                                dot += a.getDouble(i * veclen1 + v) * b.getDouble(i * veclen1 + v);
                            }
                            res.setDouble(i, dot);
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        double dot = 0;
                                        for (int v = 0; v < veclen1; v++) {
                                            dot += a.getDouble(k * veclen1 + v) * b.getDouble(k * veclen1 + v);
                                        }
                                        res.setDouble(k, dot);
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                double dot = 0;
                                for (int v = 0; v < veclen1; v++) {
                                    dot += a.getDouble(i * veclen1 + v) * b.getDouble(i * veclen1 + v);
                                }
                                res.setDouble(i, dot);
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        double[] dot = new double[2];
                        for (long i = 0; i < length; i++) {
                            dot[0] = dot[1] = 0;
                            for (int v = 0; v < veclen1; v++) {
                                double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                double[] elem_b = bc.getComplexDouble(i * veclen1 + v);
                                dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                            }
                            resc.setComplexDouble(i, dot);
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    double[] dot = new double[2];
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        dot[0] = dot[1] = 0;
                                        for (int v = 0; v < veclen1; v++) {
                                            double[] elem_a = ac.getComplexDouble(k * veclen1 + v);
                                            double[] elem_b = bc.getComplexDouble(k * veclen1 + v);
                                            dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                        }
                                        resc.setComplexDouble(k, dot);
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            double[] dot = new double[2];
                            for (long i = 0; i < length; i++) {
                                dot[0] = dot[1] = 0;
                                for (int v = 0; v < veclen1; v++) {
                                    double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                    double[] elem_b = bc.getComplexDouble(i * veclen1 + v);
                                    dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
                                }
                                resc.setComplexDouble(i, dot);
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da1.getName() + "_multD_" + da2.getName());
    }

    /**
     * Division of two DataArrays (da1 / da2) computed in a single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar / scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar / vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector / scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise division of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 / da2
     */
    public static DataArray divF(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.div(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setFloat(i * veclen2 + v, a.getFloat(i) / b.getFloat(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setFloat(k * veclen2 + v, a.getFloat(k) / b.getFloat(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setFloat(i * veclen2 + v, a.getFloat(i) / b.getFloat(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                float[] elem_a = ac.getComplexFloat(i);
                                float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                resc.setComplexFloat(i * veclen2 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            float[] elem_a = ac.getComplexFloat(k);
                                            float[] elem_b = bc.getComplexFloat(k * veclen2 + v);

                                            resc.setComplexFloat(k * veclen2 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    float[] elem_a = ac.getComplexFloat(i);
                                    float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                    resc.setComplexFloat(i * veclen2 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) / b.getFloat(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setFloat(k * veclen1 + v, a.getFloat(k * veclen1 + v) / b.getFloat(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setFloat(i * veclen1 + v, a.getFloat(i * veclen1 + v) / b.getFloat(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                float[] elem_b = bc.getComplexFloat(i);
                                resc.setComplexFloat(i * veclen1 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen1 + v);
                                            float[] elem_b = bc.getComplexFloat(k);
                                            resc.setComplexFloat(k * veclen1 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                    float[] elem_b = bc.getComplexFloat(i);
                                    resc.setComplexFloat(i * veclen1 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_divF_" + da2.getName());
    }

    /**
     * Division of two DataArrays (da1 / da2) computed in a double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar / scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar / vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector / scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise division of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1 / da2
     */
    public static DataArray divD(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                LargeArray a = td1.getValue(time);
                LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.div(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setDouble(i * veclen2 + v, a.getDouble(i) / b.getDouble(i * veclen2 + v));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setDouble(k * veclen2 + v, a.getDouble(k) / b.getDouble(k * veclen2 + v));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setDouble(i * veclen2 + v, a.getDouble(i) / b.getDouble(i * veclen2 + v));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                double[] elem_a = ac.getComplexDouble(i);
                                double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                resc.setComplexDouble(i * veclen2 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            double[] elem_a = ac.getComplexDouble(k);
                                            double[] elem_b = bc.getComplexDouble(k * veclen2 + v);
                                            resc.setComplexDouble(k * veclen2 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    double[] elem_a = ac.getComplexDouble(i);
                                    double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                    resc.setComplexDouble(i * veclen2 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) / b.getDouble(i));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setDouble(k * veclen1 + v, a.getDouble(k * veclen1 + v) / b.getDouble(k));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setDouble(i * veclen1 + v, a.getDouble(i * veclen1 + v) / b.getDouble(i));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                double[] elem_b = bc.getComplexDouble(i);
                                resc.setComplexDouble(i * veclen1 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            double[] elem_a = ac.getComplexDouble(k * veclen1 + v);
                                            double[] elem_b = bc.getComplexDouble(k);
                                            resc.setComplexDouble(k * veclen1 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                    double[] elem_b = bc.getComplexDouble(i);
                                    resc.setComplexDouble(i * veclen1 + v, LargeArrayArithmetics.complexDiv(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_divD_" + da2.getName());
    }

    /**
     * Power of DataArray (da ^ n) computed in a single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>power of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise power of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * @param n  exponent
     * 
     * @return da^n
     */
    public static DataArray powF(DataArray da, double n)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.pow(a, n, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_powF");
    }

    /**
     * Power of two DataArrays (da1 ^ da2) computed in a single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar ^ scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>element-wise power of scalar and vector</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>element-wise power of vector and scalar</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise power of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1^da2
     */
    public static DataArray powF(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.pow(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setFloat(i * veclen2 + v, (float) FastMath.pow(a.getFloat(i), b.getFloat(i * veclen2 + v)));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setFloat(k * veclen2 + v, (float) FastMath.pow(a.getFloat(k), b.getFloat(k * veclen2 + v)));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setFloat(i * veclen2 + v, (float) FastMath.pow(a.getFloat(i), b.getFloat(i * veclen2 + v)));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                float[] elem_a = ac.getComplexFloat(i);
                                float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                resc.setComplexFloat(i * veclen2 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            float[] elem_a = ac.getComplexFloat(k);
                                            float[] elem_b = bc.getComplexFloat(k * veclen2 + v);
                                            resc.setComplexFloat(k * veclen2 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    float[] elem_a = ac.getComplexFloat(i);
                                    float[] elem_b = bc.getComplexFloat(i * veclen2 + v);
                                    resc.setComplexFloat(i * veclen2 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setFloat(i * veclen1 + v, (float) FastMath.pow(a.getFloat(i * veclen1 + v), b.getFloat(i)));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setFloat(k * veclen1 + v, (float) FastMath.pow(a.getFloat(k * veclen1 + v), b.getFloat(k)));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setFloat(i * veclen1 + v, (float) FastMath.pow(a.getFloat(i * veclen1 + v), b.getFloat(i)));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                float[] elem_b = bc.getComplexFloat(i);
                                resc.setComplexFloat(i * veclen1 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen1 + v);
                                            float[] elem_b = bc.getComplexFloat(k);
                                            resc.setComplexFloat(k * veclen1 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen1 + v);
                                    float[] elem_b = bc.getComplexFloat(i);
                                    resc.setComplexFloat(i * veclen1 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_powF_" + da2.getName());
    }

    /**
     * Power of DataArray computed in a double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>power of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise power of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * @param n  exponent
     * 
     * @return da^n
     */
    public static DataArray powD(DataArray da, double n)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.pow(a, n, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_powD");
    }

    /**
     * Power of two DataArrays (da1 ^ da2) computed in a double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar ^ scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>element-wise power of scalar and vector</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>element-wise power of vector and scalar</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise power of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da1 input DataArray
     * @param da2 input DataArray
     * 
     * @return da1^da2
     */
    public static DataArray powD(DataArray da1, DataArray da2)
    {
        if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
            throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
        }
        TimeData td1, td2;
        LargeArrayType out_type;

        if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td1 = da1.getTimeData().convertToComplex();
            td2 = da2.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td1 = da1.getTimeData();
            td2 = da2.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
        ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
        timeSeries1.addAll(timeSeries2);
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen1 = da1.getVectorLength();
        final int veclen2 = da2.getVectorLength();

        if (veclen1 == veclen2) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                dataSeries.add(LargeArrayArithmetics.pow(a, b, out_type));
            }
        } else if (veclen1 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = a.length();
                final LargeArray res = LargeArrayUtils.create(out_type, b.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                res.setDouble(i * veclen2 + v, FastMath.pow(a.getDouble(i), b.getDouble(i * veclen2 + v)));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setDouble(k * veclen2 + v, FastMath.pow(a.getDouble(k), b.getDouble(k * veclen2 + v)));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    res.setDouble(i * veclen2 + v, FastMath.pow(a.getDouble(i), b.getDouble(i * veclen2 + v)));
                                }
                            }
                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen2; v++) {
                                double[] elem_a = ac.getComplexDouble(i);
                                double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                resc.setComplexDouble(i * veclen2 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            double[] elem_a = ac.getComplexDouble(k);
                                            double[] elem_b = bc.getComplexDouble(k * veclen2 + v);
                                            resc.setComplexDouble(k * veclen2 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen2; v++) {
                                    double[] elem_a = ac.getComplexDouble(i);
                                    double[] elem_b = bc.getComplexDouble(i * veclen2 + v);
                                    resc.setComplexDouble(i * veclen2 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else if (veclen2 == 1) {
            for (Float time : timeSeries) {
                final LargeArray a = td1.getValue(time);
                final LargeArray b = td2.getValue(time);
                long length = b.length();
                final LargeArray res = LargeArrayUtils.create(out_type, a.length(), false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (out_type == LargeArrayType.DOUBLE) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                res.setDouble(i * veclen1 + v, FastMath.pow(a.getDouble(i * veclen1 + v), b.getDouble(i)));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setDouble(k * veclen1 + v, FastMath.pow(a.getDouble(k * veclen1 + v), b.getDouble(k)));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    res.setDouble(i * veclen1 + v, FastMath.pow(a.getDouble(i * veclen1 + v), b.getDouble(i)));
                                }
                            }

                        }
                    }
                } else if (out_type == LargeArrayType.COMPLEX_FLOAT) {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                    final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            for (int v = 0; v < veclen1; v++) {
                                double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                double[] elem_b = bc.getComplexDouble(i);
                                resc.setComplexDouble(i * veclen1 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                            }
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            double[] elem_a = ac.getComplexDouble(k * veclen1 + v);
                                            double[] elem_b = bc.getComplexDouble(k);
                                            resc.setComplexDouble(k * veclen1 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                for (int v = 0; v < veclen1; v++) {
                                    double[] elem_a = ac.getComplexDouble(i * veclen1 + v);
                                    double[] elem_b = bc.getComplexDouble(i);
                                    resc.setComplexDouble(i * veclen1 + v, LargeArrayArithmetics.complexPow(elem_a, elem_b));
                                }
                            }
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Invalid array type.");
                }
                dataSeries.add(res);
            }
        } else {
            throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length");
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), da1.getName() + "_powD_" + da2.getName());
    }

    /**
     * Negation of DataArray computed in a single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>negation of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise negation of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return -da
     */
    public static DataArray negF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.neg(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_negF");
    }

    /**
     * Negation of DataArray computed in a double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>negation of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise negation of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return -da
     */
    public static DataArray negD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.neg(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_negD");
    }

    /**
     * Square root of DataArray in single precision. For complex arrays the principal square root is returned.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>square root of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise square root of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return sqrt(da)
     */
    public static DataArray sqrtF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.sqrt(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_sqrtF");
    }

    /**
     * Square root of DataArray in double precision. For complex arrays the principal square root is returned.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>square root of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise square root of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return sqrt(da)
     */
    public static DataArray sqrtD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.sqrt(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_sqrtD");
    }

    /**
     * Natural logarithm of DataArray in single precision. For complex arrays the principal value logarithm is returned.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>natural logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise natural logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return log(da)
     */
    public static DataArray logF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.log(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_logF");
    }

    /**
     * Natural logarithm of DataArray in double precision. For complex arrays the principal value logarithm is returned.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>natural logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise natural logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return log(da)
     */
    public static DataArray logD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.log(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_logD");
    }

    /**
     * Base 10 logarithm of DataArray in single precision. For complex arrays the principal value logarithm is returned.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>base-10 logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise base-10 logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return log10(da)
     */
    public static DataArray log10F(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.log10(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_log10F");
    }

    /**
     * Base 10 logarithm of DataArray in double precision. For complex arrays the principal value logarithm is returned.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>base-10 logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise base-10 logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return log10(da)
     */
    public static DataArray log10D(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.log10(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_log10D");
    }

    /**
     * Exponent of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>exponent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise exponent of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return exp(da)
     */
    public static DataArray expF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.exp(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_expF");
    }

    /**
     * Exponent of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>exponent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise exponent of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return exp(da)
     */
    public static DataArray expD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.exp(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_expD");
    }

    /**
     * Absolute value of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>absolute value of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>Euclidean norm of vector</td><td>scalar</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return abs(da)
     */
    public static DataArray absF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        td = da.getTimeData();
        out_type = LargeArrayType.FLOAT;
      
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        if (veclen == 1) {
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                dataSeries.add(LargeArrayArithmetics.abs(a, out_type));
            }
        } else {
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                long length = a.length() / veclen;
                final LargeArray res = LargeArrayUtils.create(out_type, length, false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (a.getType() != LargeArrayType.COMPLEX_FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            double norm = 0;
                            for (int v = 0; v < veclen; v++) {
                                norm += a.getFloat(i * veclen + v) * a.getFloat(i * veclen + v);
                            }
                            res.setFloat(i, (float) FastMath.sqrt(norm));
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        double norm = 0;
                                        for (int v = 0; v < veclen; v++) {
                                            norm += a.getFloat(k * veclen + v) * a.getFloat(k * veclen + v);
                                        }
                                        res.setFloat(k, (float) FastMath.sqrt(norm));
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                double norm = 0;
                                for (int v = 0; v < veclen; v++) {
                                    norm += a.getFloat(i * veclen + v) * a.getFloat(i * veclen + v);
                                }
                                res.setFloat(i, (float) FastMath.sqrt(norm));
                            }
                        }
                    }
                } else {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final FloatLargeArray resc = (FloatLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            double norm = 0;
                            for (int v = 0; v < veclen; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen + v);
                                norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                            }
                            resc.setFloat(i, (float) FastMath.sqrt(norm));
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        double norm = 0;
                                        for (int v = 0; v < veclen; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen + v);
                                            norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                                        }
                                        resc.setFloat(k, (float) FastMath.sqrt(norm));
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                double norm = 0;
                                for (int v = 0; v < veclen; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen + v);
                                    norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                                }
                                resc.setFloat(i, (float) FastMath.sqrt(norm));
                            }
                        }
                    }
                }
                dataSeries.add(res);
            }
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), 1, da.getName() + "_absF");
    }

    /**
     * Absolute value of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>absolute value of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>Euclidean norm of vector</td><td>scalar</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return abs(da)
     */
    public static DataArray absD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        td = da.getTimeData();
        out_type = LargeArrayType.DOUBLE;

        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        if (veclen == 1) {
            for (Float time : timeSeries) {
                LargeArray a = td.getValue(time);
                dataSeries.add(LargeArrayArithmetics.abs(a, out_type));
            }
        } else {
            for (Float time : timeSeries) {
                final LargeArray a = td.getValue(time);
                long length = a.length() / veclen;
                final LargeArray res = LargeArrayUtils.create(out_type, length, false);
                int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
                if (a.getType() != LargeArrayType.COMPLEX_FLOAT) {
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            double norm = 0;
                            for (int v = 0; v < veclen; v++) {
                                norm += a.getDouble(i * veclen + v) * a.getDouble(i * veclen + v);
                            }
                            res.setDouble(i, FastMath.sqrt(norm));
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        double norm = 0;
                                        for (int v = 0; v < veclen; v++) {
                                            norm += a.getDouble(k * veclen + v) * a.getDouble(k * veclen + v);
                                        }
                                        res.setDouble(k, FastMath.sqrt(norm));
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                double norm = 0;
                                for (int v = 0; v < veclen; v++) {
                                    norm += a.getDouble(i * veclen + v) * a.getDouble(i * veclen + v);
                                }
                                res.setDouble(i, FastMath.sqrt(norm));
                            }
                        }
                    }
                } else {
                    final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                    final DoubleLargeArray resc = (DoubleLargeArray) res;
                    if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                        for (long i = 0; i < length; i++) {
                            double norm = 0;
                            for (int v = 0; v < veclen; v++) {
                                float[] elem_a = ac.getComplexFloat(i * veclen + v);
                                norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                            }
                            resc.setDouble(i, FastMath.sqrt(norm));
                        }
                    } else {
                        long k = length / nthreads;
                        Future[] threads = new Future[nthreads];
                        for (int j = 0; j < nthreads; j++) {
                            final long firstIdx = j * k;
                            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                            threads[j] = ConcurrencyUtils.submit(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    for (long k = firstIdx; k < lastIdx; k++) {
                                        double norm = 0;
                                        for (int v = 0; v < veclen; v++) {
                                            float[] elem_a = ac.getComplexFloat(k * veclen + v);
                                            norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                                        }
                                        resc.setDouble(k, FastMath.sqrt(norm));
                                    }
                                }
                            });
                        }
                        try {
                            ConcurrencyUtils.waitForCompletion(threads);
                        } catch (InterruptedException | ExecutionException ex) {
                            for (long i = 0; i < length; i++) {
                                double norm = 0;
                                for (int v = 0; v < veclen; v++) {
                                    float[] elem_a = ac.getComplexFloat(i * veclen + v);
                                    norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                                }
                                resc.setDouble(i, FastMath.sqrt(norm));
                            }
                        }
                    }
                }
                dataSeries.add(res);
            }
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), 1, da.getName() + "_absD");
    }

    /**
     * Sine of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return sin(da)
     */
    public static DataArray sinF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.sin(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_sinF");
    }

    /**
     * Sine of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     *
     * @param da input DataArray
     * 
     * @return sin(da)
     */
    public static DataArray sinD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.sin(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_sinD");
    }

    /**
     * Cosine of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     *
     * @param da input DataArray
     * 
     * @return cos(da)
     */
    public static DataArray cosF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.cos(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_cosF");
    }

    /**
     * Cosine of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return cos(da)
     */
    public static DataArray cosD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.cos(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_cosD");
    }

    /**
     * Tangent of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return tan(da)
     */
    public static DataArray tanF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.tan(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_tanF");
    }

    /**
     * Tangent of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return tan(da)
     */
    public static DataArray tanD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.tan(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_tanD");
    }

    /**
     * Inverse sine of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return asin(da)
     */
    public static DataArray asinF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.asin(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_asinF");
    }

    /**
     * Inverse sine of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return asin(da)
     */
    public static DataArray asinD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.asin(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_asinD");
    }

    /**
     * Inverse cosine of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return acos(da)
     */
    public static DataArray acosF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.acos(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_acosF");
    }

    /**
     * Inverse cosine of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return acos(da)
     */
    public static DataArray acosD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.acos(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_cosD");
    }

    /**
     * Inverse tangent of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return atan(da)
     */
    public static DataArray atanF(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.FLOAT;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.atan(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_atanF");
    }

    /**
     * Inverse tangent of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return atan(da)
     */
    public static DataArray atanD(DataArray da)
    {
        if (da == null || !da.isNumeric()) {
            throw new IllegalArgumentException("da == null || !da.isNumeric()");
        }
        TimeData td;
        LargeArrayType out_type;

        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            td = da.getTimeData().convertToComplex();
            out_type = LargeArrayType.COMPLEX_FLOAT;
        } else {
            td = da.getTimeData();
            out_type = LargeArrayType.DOUBLE;
        }
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.atan(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_atanD");
    }

    /**
     * Signum of DataArray in single precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>signum of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise signum of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return signum(da)
     */
    public static DataArray signumF(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td;
        LargeArrayType out_type;

        td = da.getTimeData();
        out_type = LargeArrayType.FLOAT;

        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.signum(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_signumF");
    }

    /**
     * Signum of DataArray in double precision.
     * 
     * <table border="1"  summary="">
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>signum of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise signum of vector</td><td>vector</td>
     * </tr>
     * </table>
     * 
     * @param da input DataArray
     * 
     * @return signum(da)
     */
    public static DataArray signumD(DataArray da)
    {
        if (da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            throw new IllegalArgumentException("da == null || !da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_COMPLEX");
        }
        TimeData td;
        LargeArrayType out_type;

        td = da.getTimeData();
        out_type = LargeArrayType.DOUBLE;

        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = da.getVectorLength();

        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            dataSeries.add(LargeArrayArithmetics.signum(a, out_type));
        }
        return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, da.getName() + "_signumD");
    }
}
