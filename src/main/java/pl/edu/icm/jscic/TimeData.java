/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.utils.ArrayUtils;
import static pl.edu.icm.jscic.utils.ConvertUtils.*;
import static pl.edu.icm.jscic.utils.CropDownUtils.*;
import pl.edu.icm.jscic.utils.FloatingPointUtils;
import pl.edu.icm.jscic.utils.InfinityAction;
import pl.edu.icm.jscic.utils.NaNAction;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.ObjectLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.StringLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jlargearrays.LongLargeArray;
import pl.edu.icm.jscic.utils.SliceUtils;

/**
 *
 * Holds a series of data for various moments of time. Time interpolation is
 * supported in the case of basic numerical type arrays.
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TimeData implements Serializable
{

    private static final long serialVersionUID = -6758651690091908261L;

    /**
     * Enumeration used for testing if a time moment is within time range. EMPTY
     * if no timesteps are present. BEFORE if the time parameter is before the
     * earliest stored moment. IN if time is within the range of available
     * timesteps. AFTER - when after all the stored timesteps.
     */
    public static enum Position
    {

        BEFORE, IN, AFTER, EMPTY
    };

    /**
     * Type of data array.
     */
    private DataArrayType type;

    /**
     * Array holding moments in time.
     */
    private ArrayList<Float> timeSeries = new ArrayList<>();

    /**
     * Array holding data series.
     */
    private ArrayList<LargeArray> dataSeries = new ArrayList<>();

    /**
     * Current moment in time.
     */
    private float currentTime = 0;

    /**
     * Data corresponding to the current moment in time.
     */
    private LargeArray data;

    /**
     * NaN action.
     */
    private NaNAction nanAction;

    /**
     * Infinity action.
     */
    private InfinityAction infAction;

    /**
     * Creates a new instance of TimeData.
     *
     * @param type data array type
     */
    public TimeData(DataArrayType type)
    {
        if (type == DataArrayType.FIELD_DATA_UNKNOWN) {
            throw new IllegalArgumentException("Unknown data type");
        }
        this.type = type;
        this.nanAction = FloatingPointUtils.defaultNanAction;
        this.infAction = FloatingPointUtils.defaultInfinityAction;
    }

    /**
     * Creates a new instance of TimeData.
     *
     * @param timeSeries  moments in time
     * @param dataSeries  data series
     * @param currentTime current moment in time
     */
    public TimeData(ArrayList<Float> timeSeries, ArrayList<LargeArray> dataSeries, float currentTime)
    {
        this(timeSeries, dataSeries, currentTime, true, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Creates a new instance of TimeData.
     *
     * @param timeSeries  moments in time
     * @param dataSeries  data series
     * @param currentTime current moment in time
     * @param testNanInf  if true, then data series are tested for NaNs and
     *                    Infinities
     * @param nanAction   NaN action
     * @param infAction   Infinity action
     */
    public TimeData(ArrayList<Float> timeSeries, ArrayList<LargeArray> dataSeries, float currentTime, boolean testNanInf, NaNAction nanAction, InfinityAction infAction)
    {
        if (timeSeries == null || dataSeries == null || timeSeries.isEmpty() || dataSeries.isEmpty() || timeSeries.size() != dataSeries.size()) {
            throw new IllegalArgumentException("timeSeries == null || dataSeries == null || timeSeries.isEmpty() || dataSeries.isEmpty() || timeSeries.size() != dataSeries.size()");
        }
        if (!ArrayUtils.isSorted(timeSeries)) {
            throw new IllegalArgumentException("The timeSeries list is not sorted");
        }
        LargeArray d0 = dataSeries.get(0);
        switch (d0.getType()) {
            case LOGIC:
                type = DataArrayType.FIELD_DATA_LOGIC;
                break;
            case UNSIGNED_BYTE:
                type = DataArrayType.FIELD_DATA_BYTE;
                break;
            case SHORT:
                type = DataArrayType.FIELD_DATA_SHORT;
                break;
            case INT:
                type = DataArrayType.FIELD_DATA_INT;
                break;
            case LONG:
                type = DataArrayType.FIELD_DATA_LONG;
                break;
            case FLOAT:
                type = DataArrayType.FIELD_DATA_FLOAT;
                for (int i = 0; i < dataSeries.size(); i++) {
                    d0 = dataSeries.get(i);
                    if (testNanInf) {
                        FloatingPointUtils.processNaNs((FloatLargeArray) d0, nanAction, infAction);
                    }
                }
                break;
            case DOUBLE:
                type = DataArrayType.FIELD_DATA_DOUBLE;
                for (int i = 0; i < dataSeries.size(); i++) {
                    d0 = dataSeries.get(i);
                    if (testNanInf) {
                        FloatingPointUtils.processNaNs((DoubleLargeArray) d0, nanAction, infAction);
                    }
                }
                break;
            case COMPLEX_FLOAT:
                type = DataArrayType.FIELD_DATA_COMPLEX;
                for (int i = 0; i < dataSeries.size(); i++) {
                    d0 = dataSeries.get(i);
                    if (testNanInf) {
                        FloatingPointUtils.processNaNs((ComplexFloatLargeArray) d0, nanAction, infAction);
                    }
                }
                break;
            case STRING:
                type = DataArrayType.FIELD_DATA_STRING;
                break;
            case OBJECT:
                type = DataArrayType.FIELD_DATA_OBJECT;
                break;
            default:
                throw new IllegalArgumentException("Invalid type of dataSeries argument.");
        }
        this.timeSeries = timeSeries;
        this.dataSeries = dataSeries;
        setCurrentTime(currentTime);
        this.nanAction = nanAction;
        this.infAction = infAction;
    }

    /**
     * Returns a shallow copy of this instance.
     *
     * @return a shallow copy of this instance
     */
    public TimeData cloneShallow()
    {
        if (timeSeries.isEmpty()) {
            TimeData clone = new TimeData(type);
            clone.currentTime = currentTime;
            clone.nanAction = nanAction;
            clone.infAction = infAction;
            return clone;
        } else {
            return new TimeData((ArrayList<Float>) timeSeries.clone(), (ArrayList<LargeArray>) dataSeries.clone(), currentTime);
        }
    }

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public TimeData cloneDeep()
    {
        if (timeSeries.isEmpty()) {
            TimeData clone = new TimeData(type);
            clone.currentTime = currentTime;
            clone.nanAction = nanAction;
            clone.infAction = infAction;
            return clone;
        } else {
            ArrayList<Float> timeSeriesClone = new ArrayList<>(timeSeries.size());
            ArrayList<LargeArray> dataSeriesClone = new ArrayList<>(dataSeries.size());
            for (int i = 0; i < dataSeries.size(); i++) {
                timeSeriesClone.add(timeSeries.get(i));
                dataSeriesClone.add((LargeArray) dataSeries.get(i).clone());
            }
            return new TimeData(timeSeriesClone, dataSeriesClone, currentTime);
        }
    }

    /**
     * Time steps compatibility check.
     *
     * @param t TimeData to compare with
     *
     * @return true if moments in time vectors are identical, false if they
     *         differ in length or content
     */
    public boolean isTimeCompatibleWith(TimeData t)
    {
        if (t == null) {
            return false;
        }
        ArrayList<Float> sMoments = t.getTimesAsList();
        if (sMoments.size() != timeSeries.size()) {
            return false;
        }
        for (int i = 0; i < timeSeries.size(); i++) {
            if (!Objects.equals(timeSeries.get(i), sMoments.get(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Set the data at a given moment in time.
     *
     * @param d0 new data
     * @param t  new moment in time
     */
    public void setValue(LargeArray d0, float t)
    {
        int k = -1;
        if (d0 == null) {
            throw new IllegalArgumentException("d0 cannot be null.");
        }
        if (!isCompatibleWithDataType(d0)) {
            throw new IllegalArgumentException("Invalid type of dataSeries argument.");
        }
        if (type == DataArrayType.FIELD_DATA_FLOAT) {
            FloatingPointUtils.processNaNs((FloatLargeArray) d0, nanAction, infAction);
        } else if (type == DataArrayType.FIELD_DATA_DOUBLE) {
            FloatingPointUtils.processNaNs((DoubleLargeArray) d0, nanAction, infAction);
        } else if (type == DataArrayType.FIELD_DATA_COMPLEX) {
            FloatingPointUtils.processNaNs((ComplexFloatLargeArray) d0, nanAction, infAction);
        }

        for (int i = 0; i < timeSeries.size(); i++) {
            if (timeSeries.get(i) >= t) {
                k = i;
                break;
            }
        }

        if (k == -1) {
            timeSeries.add(t);
            dataSeries.add(d0);
        } else if (timeSeries.get(k) == t) {
            dataSeries.set(k, d0);
        } else {
            timeSeries.add(k, t);
            dataSeries.add(k, d0);
        }
        if (dataSeries.size() == 1 || t == currentTime) {
            data = d0;
        } else if (currentTime != t) {
            float previous = -1, next = -1;
            for (int i = 0; i < timeSeries.size(); i++) {
                if (timeSeries.get(i) >= currentTime) {
                    previous = timeSeries.get(max(0, i - 1));
                    next = timeSeries.get(min(timeSeries.size() - 1, i + 1));
                    break;
                }
            }
            if (previous == t || next == t) {
                data = getValue(currentTime);
            }
        }
    }

    /**
     * Removes the data at a given moment in time.
     *
     * @param t new moment in time
     */
    public void removeValue(float t)
    {
        int k = -1;
        for (int i = 0; i < timeSeries.size(); i++) {
            if (timeSeries.get(i) >= t) {
                k = i;
                break;
            }
        }

        if (k >= 0) {
            timeSeries.remove(k);
            dataSeries.remove(k);
            if (t == currentTime) {
                if (timeSeries.size() == 1) {
                    setCurrentTime(timeSeries.get(0));
                } else if (timeSeries.size() > 1) {
                    setCurrentTime(timeSeries.get(min(k + 1, timeSeries.size() - 1)));
                } else {
                    setCurrentTime(0);
                }
            } else {
                float previous = -1, next = -1;
                for (int i = 0; i < timeSeries.size(); i++) {
                    if (timeSeries.get(i) >= currentTime) {
                        previous = timeSeries.get(max(0, i - 1));
                        next = timeSeries.get(min(timeSeries.size() - 1, i + 1));
                        break;
                    }
                }
                if (previous == t || next == t) {
                    data = getValue(currentTime);
                }
            }
        }
    }

    /**
     * Returns the type of data array.
     *
     * @return type of data array
     */
    public DataArrayType getType()
    {
        return type;
    }

    /**
     * Returns the length of each data array. -1 is returned when the time data
     * is empty.
     *
     * @return length of each data array.
     */
    public long length()
    {
        if (dataSeries.isEmpty()) {
            return -1;
        }
        return dataSeries.get(0).length();
    }

    /**
     * Checks if a given time moment is within time range.
     *
     * @param time moment in time
     *
     * @return EMPTY if no timesteps are present, BEFORE if the time parameter
     *         is before the earliest stored moment, IN if time is within the range of
     *         available timesteps, AFTER - when after all the stored timesteps
     */
    public Position getTimePosition(float time)
    {
        if (timeSeries.isEmpty()) {
            return Position.EMPTY;
        }
        if (time < timeSeries.get(0)) {
            return Position.BEFORE;
        } else if (time > timeSeries.get(timeSeries.size() - 1)) {
            return Position.AFTER;
        } else {
            return Position.IN;
        }
    }

    /**
     * Set the current moment in time to a given value, recomputes (selects or
     * interpolates) the values of current data.
     *
     * @param time new value of current moment in time
     *
     * @return EMPTY if no timesteps are present, BEFORE if the time parameter
     *         is before the earliest stored moment, IN if time is within the range of
     *         available timesteps, AFTER - when after all the stored timesteps
     */
    public final Position setCurrentTime(float time)
    {
        if (time != currentTime || data == null) {
            currentTime = time;
            data = getValue(time);
        }
        return getTimePosition(time);
    }

    /**
     * Returns current moment in time.
     *
     * @return current moment in time
     */
    public float getCurrentTime()
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty.");
        }
        return currentTime;
    }

    /**
     * Adds the given data array at the end of this time data.
     *
     * @param d data array
     */
    public void addValue(LargeArray d)
    {
        if (timeSeries.isEmpty()) {
            setValue(d, 0);
        } else {
            setValue(d, getEndTime() + 1);
        }
        data = d;
    }

    /**
     * Returns true if a given moment is a time step, false otherwise
     *
     * @param t moment in time
     *
     * @return true if a given moment is a time step, false otherwise
     */
    public boolean isTimestep(float t)
    {
        for (int i = 0; i < timeSeries.size(); i++) {
            if (t == timeSeries.get(i)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns data array corresponding to the current time step. This method
     * always returns a reference to the internal array.
     *
     * @return data array corresponding to the current time step
     */
    public LargeArray getCurrentValue()
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty.");
        }
        return data;
    }

    /**
     * Returns data array corresponding to a given moment if it is a time step.
     * Otherwise it creates a new times step at a given moment and returns an
     * empty data array.
     *
     * @param time moment in time
     * @param n    the length of data array
     *
     * @return data array corresponding to a given moment
     */
    public LargeArray produceValue(float time, long n)
    {
        for (int i = 0; i < timeSeries.size(); i++) {
            if (timeSeries.get(i) == time) {
                return dataSeries.get(i);
            }
        }
        LargeArray newData = null;
        switch (type) {
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_BYTE:
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_LONG:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
            case FIELD_DATA_COMPLEX:
                newData = LargeArrayUtils.create(type.toLargeArrayType(), n);
                break;
            case FIELD_DATA_STRING:
                newData = (LargeArray) (new StringLargeArray(n, DataArray.MAX_STRING_LENGTH));
                break;
            case FIELD_DATA_OBJECT:
                newData = (LargeArray) (new ObjectLargeArray(n, DataArray.MAX_OBJECT_LENGTH));
                break;
            default:
                throw new IllegalStateException("Invalid type");
        }

        setValue(newData, time);
        return newData;

    }

    /**
     * Returns a position in the time series array corresponding to a given
     * moment. Returns -1 if time series is empty or a given moment is not a
     * time step.
     *
     * @param time moment in time
     *
     * @return a position in the time series array corresponding to a given
     *         moment
     */
    public int getStep(float time)
    {
        if (timeSeries.isEmpty()) {
            return -1;
        }
        for (int i = 0; i < timeSeries.size(); i++) {
            if (timeSeries.get(i) == time) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns data array corresponding to a given moment if it is a time step.
     * Otherwise it returns interpolated data array (linear interpolation for
     * numeric types, nearest-neighbor interpolation otherwise).
     *
     * @param time moment in time
     *
     * @return data array corresponding to a given moment
     */
    public LargeArray getValue(float time)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }

        for (int i = 0; i < timeSeries.size(); i++) {
            if (timeSeries.get(i) == time) {
                return dataSeries.get(i);
            }
        }
        int k = -1;
        LargeArray outData;
        for (int i = 0; i < timeSeries.size(); i++) {
            if (timeSeries.get(i) >= time) {
                k = i;
                break;
            }
        }
        if (k == -1) {
            outData = dataSeries.get(dataSeries.size() - 1);
        } else if (k == 0 || timeSeries.get(k) == time) {
            outData = dataSeries.get(k);
        } else {
            LargeArray d0 = dataSeries.get(k - 1);
            LargeArray d1 = dataSeries.get(k);
            float u = (time - timeSeries.get(k - 1)) / (timeSeries.get(k) - timeSeries.get(k - 1));
            long n = min(d0.length(), d1.length());
            switch (d0.getType()) {
                case UNSIGNED_BYTE:
                case SHORT:
                case INT:
                case LONG:
                case FLOAT:
                case DOUBLE:
                    outData = LargeArrayUtils.create(d0.getType(), n, false);
                    for (long i = 0; i < n; i++) {
                        outData.setDouble(i, u * d1.getDouble(i) + (1 - u) * d0.getDouble(i));
                    }
                    break;
                case COMPLEX_FLOAT: {
                    ComplexFloatLargeArray dd0 = (ComplexFloatLargeArray) d0;
                    ComplexFloatLargeArray dd1 = (ComplexFloatLargeArray) d1;
                    ComplexFloatLargeArray outd = new ComplexFloatLargeArray(n);
                    for (long i = 0; i < n; i++) {
                        float[] d1v = dd1.getComplexFloat(i);
                        float[] d0v = dd0.getComplexFloat(i);
                        float[] resv = new float[]{u * d1v[0] + (1 - u) * d0v[0], u * d1v[1] + (1 - u) * d0v[1]};
                        outd.setComplexFloat(i, resv);
                    }
                    outData = (LargeArray) outd;
                    break;
                }
                default:
                    if (u < .5) {
                        outData = d0;
                    } else {
                        outData = d1;
                    }
            }
        }
        return outData;
    }

    /**
     * Returns the list of data arrays for all time steps. This method always
     * returns a reference to the internal list.
     *
     * @return list of data arrays for all time steps
     */
    public ArrayList<LargeArray> getValues()
    {
        return dataSeries;
    }

    /**
     * Returns the number of time steps.
     *
     * @return number of time steps
     */
    public int getNSteps()
    {
        return dataSeries.size();
    }

    /**
     * Returns true if time series is empty, false otherwise
     *
     * @return true if time series is empty, false otherwise
     */
    public boolean isEmpty()
    {
        return dataSeries.isEmpty();
    }

    /**
     * Returns the time step corresponding to a given position in the time
     * series array.
     *
     * @param frame position in the time series array
     *
     * @return time step corresponding to a given position in the time series
     *         array
     */
    public float getTime(int frame)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }

        if (frame < 0) {
            frame = 0;
        }
        if (frame > timeSeries.size() - 1) {
            frame = timeSeries.size() - 1;
        }

        return timeSeries.get(frame);
    }

    /**
     * Returns the first time step.
     *
     * @return first time step
     */
    public float getStartTime()
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }

        return timeSeries.get(0);
    }

    /**
     * Returns the last time step.
     *
     * @return last time step
     */
    public float getEndTime()
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }

        return timeSeries.get(timeSeries.size() - 1);
    }

    /**
     * Removes all timesteps and values.
     */
    public void clear()
    {
        timeSeries.clear();
        dataSeries.clear();
        currentTime = -1;
        data = null;
    }

    /**
     * Returns all times steps as list. This method always returns a reference
     * to the internal list.
     *
     * @return all times steps as list
     */
    public ArrayList<Float> getTimesAsList()
    {
        return timeSeries;
    }

    /**
     * Returns all times steps as array.
     *
     * @return all times steps as array
     */
    public float[] getTimesAsArray()
    {
        float[] out = new float[timeSeries.size()];
        for (int i = 0; i < out.length; i++) {
            out[i] = timeSeries.get(i);
        }
        return out;
    }

    /**
     * Crops and downsizes this TimeData.
     *
     * @param veclen vector length
     * @param dims   dimensions of this TimeData
     * @param low    amount of cropping from the beginning in each dimension
     * @param up     amount of cropping from the end in each dimension
     * @param down   amount of downsizing in each dimension
     *
     * @return cropped and downsized TimeData
     */
    public TimeData cropDown(int veclen, int[] dims, int[] low, int[] up, int[] down)
    {

        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData td = new TimeData(type);

        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            td.setValue((LargeArray) cropDownArray(dataSeries.get(timestep), veclen, dims, low, up, down), timeSeries.get(timestep));
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * Converts this TimeData to TimeData of type byte.
     *
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize = true,
     *                  it cannot be larger than the minimum of the TimeData value
     * @param max       maximal value for normalization used only if normalize = true,
     *                  it cannot be smaller than the maximum of the TimeData value
     *
     * @return byte TimeData
     */
    public TimeData convertToByte(boolean normalize, float min, float max)
    {
        if (this.getType() == DataArrayType.FIELD_DATA_BYTE) {
            return this;
        }
        TimeData td = new TimeData(DataArrayType.FIELD_DATA_BYTE);
        if (dataSeries.isEmpty()) {
            return td;
        }
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            LargeArray d = dataSeries.get(timestep);
            switch (type) {
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_COMPLEX:
                    td.setValue(convertToUnsignedByteLargeArray(dataSeries.get(timestep), normalize, min, max), timeSeries.get(timestep));
                    break;
                default:
                    td.setValue(convertToUnsignedByteLargeArray(d), timeSeries.get(timestep));
            }
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * Converts this TimeData to TimeData of type short.
     *
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize = true,
     *                  it cannot be larger than the minimum of the TimeData value
     * @param max       maximal value for normalization used only if normalize = true,
     *                  it cannot be smaller than the maximum of the TimeData value
     *
     * @return short TimeData
     */
    public TimeData convertToShort(boolean normalize, float min, float max)
    {
        if (this.getType() == DataArrayType.FIELD_DATA_SHORT) {
            return this;
        }
        TimeData td = new TimeData(DataArrayType.FIELD_DATA_SHORT);
        if (dataSeries.isEmpty()) {
            return td;
        }
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            LargeArray d = dataSeries.get(timestep);
            switch (type) {
                case FIELD_DATA_INT:
                case FIELD_DATA_LONG:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_COMPLEX:
                    td.setValue(convertToShortLargeArray(dataSeries.get(timestep), normalize, min, max), timeSeries.get(timestep));
                    break;
                default:
                    td.setValue(convertToShortLargeArray(d), timeSeries.get(timestep));
            }
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * Converts this TimeData to TimeData of type int.
     *
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize = true,
     *                  it cannot be larger than the minimum of the TimeData value
     * @param max       maximal value for normalization used only if normalize = true,
     *                  it cannot be smaller than the maximum of the TimeData value
     *
     * @return int TimeData
     */
    public TimeData convertToInt(boolean normalize, float min, float max)
    {
        if (this.getType() == DataArrayType.FIELD_DATA_INT) {
            return this;
        }
        TimeData td = new TimeData(DataArrayType.FIELD_DATA_INT);
        if (dataSeries.isEmpty()) {
            return td;
        }
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            LargeArray d = dataSeries.get(timestep);
            switch (type) {
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_COMPLEX:
                    td.setValue(convertToIntLargeArray(dataSeries.get(timestep), normalize, min, max), timeSeries.get(timestep));
                    break;
                default:
                    td.setValue(convertToIntLargeArray(d), timeSeries.get(timestep));
            }
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * Converts this TimeData to TimeData of type long.
     *
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize = true,
     *                  it cannot be larger than the minimum of the TimeData value
     * @param max       maximal value for normalization used only if normalize = true,
     *                  it cannot be smaller than the maximum of the TimeData value
     *
     * @return long TimeData
     */
    public TimeData convertToLong(boolean normalize, float min, float max)
    {
        if (this.getType() == DataArrayType.FIELD_DATA_LONG) {
            return this;
        }
        TimeData td = new TimeData(DataArrayType.FIELD_DATA_LONG);
        if (dataSeries.isEmpty()) {
            return td;
        }
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            LargeArray d = dataSeries.get(timestep);
            switch (type) {
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE:
                case FIELD_DATA_COMPLEX:
                    td.setValue(convertToLongLargeArray(dataSeries.get(timestep), normalize, min, max), timeSeries.get(timestep));
                    break;
                default:
                    td.setValue(convertToLongLargeArray(d), timeSeries.get(timestep));
            }
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * Converts this TimeData to TimeData of type float.
     *
     * @return float TimeData
     */
    public TimeData convertToFloat()
    {
        if (this.getType() == DataArrayType.FIELD_DATA_FLOAT) {
            return this;
        }
        TimeData td = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        if (dataSeries.isEmpty()) {
            return td;
        }
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            td.setValue(convertToFloatLargeArray(dataSeries.get(timestep)), timeSeries.get(timestep));
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * Converts this TimeData to TimeData of type double.
     *
     * @return double TimeData
     */
    public TimeData convertToDouble()
    {
        if (this.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
            return this;
        }
        TimeData td = new TimeData(DataArrayType.FIELD_DATA_DOUBLE);
        if (dataSeries.isEmpty()) {
            return td;
        }
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            td.setValue(convertToDoubleLargeArray(dataSeries.get(timestep)), timeSeries.get(timestep));
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * Converts this TimeData to TimeData of type complex.
     *
     * @return double TimeData
     */
    public TimeData convertToComplex()
    {
        if (this.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            return this;
        }
        TimeData td = new TimeData(DataArrayType.FIELD_DATA_COMPLEX);
        if (dataSeries.isEmpty()) {
            return td;
        }
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            td.setValue(convertToComplexFloatLargeArray(dataSeries.get(timestep)), timeSeries.get(timestep));
        }
        td.setCurrentTime(currentTime);
        return td;
    }

    /**
     * For each time step concatenates the data arrays from a given list of
     * TimeData with this TimeData. Concatenated data will have the same time
     * steps as this TimeData. At each time step, the data array will be of
     * length: this.length() + Sum(i=0,concatenatadData.size()-1,
     * concatenatadData.get(i).length()).
     *
     * @param concatenatedData TimeData to be concatenated with this TimeData
     *
     * @return concatenated TimeData
     */
    public TimeData concatenate(ArrayList<TimeData> concatenatedData)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData out = new TimeData(type);
        long n = length();
        for (int i = 0; i < concatenatedData.size(); i++) {
            TimeData cD = concatenatedData.get(i);
            if (!isTimeCompatibleWith(cD)) {
                throw new IllegalArgumentException("Incompatible time series.");
            }
            if (!isCompatibleWithDataType(cD.getCurrentValue())) {
                throw new IllegalArgumentException("Incompatible type.");
            }
            n += cD.length();
        }
        LargeArray d = dataSeries.get(0);
        for (int i = 0; i < getNSteps(); i++) {
            long k = 0;
            LargeArray outData = LargeArrayUtils.create(type.toLargeArrayType(), n, false);
            LargeArrayUtils.arraycopy(d, 0, outData, k, length());
            k += length();
            for (int j = 0; j < concatenatedData.size(); j++) {
                TimeData cD = concatenatedData.get(j);
                LargeArrayUtils.arraycopy(cD.getValues().get(i), 0, outData, k, cD.length());
                k += cD.length();
            }
            out.setValue(outData, getTime(i));
        }
        return out;
    }

    /**
     * Returns a 1D slice of this TimeData.
     *
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D slice
     */
    public TimeData get1DSlice(long start, long n, long step, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData s = new TimeData(type);
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            s.setValue(SliceUtils.get1DSlice(dataSeries.get(timestep), start, n, step, (long) veclen), timeSeries.get(timestep));
        }
        s.setCurrentTime(currentTime);
        return s;
    }

    /**
     * Returns a current 1D slice of this TimeData.
     *
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return current 1D slice
     */
    public LargeArray getCurrent1DSlice(long start, long n, long step, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        return SliceUtils.get1DSlice(getCurrentValue(), start, n, step, (long) veclen);
    }

    /**
     * Returns a 1D float slice of this TimeData.
     *
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D float slice
     */
    public TimeData get1DFloatSlice(long start, long n, long step, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData s = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            s.setValue(SliceUtils.get1DFloatSlice(dataSeries.get(timestep), start, n, step, (long) veclen), timeSeries.get(timestep));
        }
        s.setCurrentTime(currentTime);
        return s;
    }

    /**
     * Returns a current 1D float slice of this TimeData.
     *
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return current 1D float slice
     */
    public FloatLargeArray getCurrent1DFloatSlice(long start, long n, long step, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        return SliceUtils.get1DFloatSlice(getCurrentValue(), start, n, step, (long) veclen);
    }

    /**
     * Returns a 1D slice of norms of this TimeData.
     *
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D slice of norms
     */
    public TimeData get1DNormSlice(long start, long n, long step, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData s = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            s.setValue(SliceUtils.get1DNormSlice(dataSeries.get(timestep), start, n, step, (long) veclen), timeSeries.get(timestep));
        }
        s.setCurrentTime(currentTime);
        return s;
    }

    /**
     * Returns a current 1D slice of norms of this TimeData.
     *
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return current 1D slice of norms
     */
    public FloatLargeArray getCurrent1DNormSlice(long start, long n, long step, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        return SliceUtils.get1DNormSlice(getCurrentValue(), start, n, step, (long) veclen);
    }

    /**
     * Returns a 2D slice of this TimeData.
     *
     * @param dims   dimensions of each time value
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in each time value
     * @param veclen vector length of each time value
     *
     * @return 2D slice
     */
    public TimeData get2DSlice(long[] dims, int axis, long slice, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData s = new TimeData(type);
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            s.setValue(SliceUtils.get2DSlice(dataSeries.get(timestep), dims, axis, slice, (long) veclen), timeSeries.get(timestep));
        }
        s.setCurrentTime(currentTime);
        return s;
    }

    /**
     * Returns a current 2D slice of this TimeData.
     *
     * @param dims   dimensions of each time value
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in each time value
     * @param veclen vector length of each time value
     *
     * @return current 2D slice
     */
    public LargeArray getCurrent2DSlice(long[] dims, int axis, long slice, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        return SliceUtils.get2DSlice(getCurrentValue(), dims, axis, slice, (long) veclen);
    }

    /**
     * Returns a 2D float slice of this TimeData.
     *
     * @param dims   dimensions of each time value
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in each time value
     * @param veclen vector length of each time value
     *
     * @return 2D float slice
     */
    public TimeData get2DFloatSlice(long[] dims, int axis, long slice, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData s = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            s.setValue(SliceUtils.get2DFloatSlice(dataSeries.get(timestep), dims, axis, slice, (long) veclen), timeSeries.get(timestep));
        }
        s.setCurrentTime(currentTime);
        return s;
    }

    /**
     * Returns a current 2D float slice of this TimeData.
     *
     * @param dims   dimensions of each time value
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in each time value
     * @param veclen vector length of each time value
     *
     * @return current 2D float slice
     */
    public FloatLargeArray getCurrent2DFloatSlice(long[] dims, int axis, long slice, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        return SliceUtils.get2DFloatSlice(getCurrentValue(), dims, axis, slice, (long) veclen);
    }

    /**
     * Returns a 2D slice of norms of this TimeData.
     *
     * @param dims   dimensions of each time value
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in each time value
     * @param veclen vector length of each time value
     *
     * @return 2D slice of norms
     */
    public TimeData get2DNormSlice(long[] dims, int axis, long slice, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        TimeData s = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        for (int timestep = 0; timestep < dataSeries.size(); timestep++) {
            s.setValue(SliceUtils.get2DNormSlice(dataSeries.get(timestep), dims, axis, slice, (long) veclen), timeSeries.get(timestep));
        }
        s.setCurrentTime(currentTime);
        return s;
    }

    /**
     * Returns a current 2D slice of norms of this TimeData.
     *
     * @param dims   dimensions of each time value
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in each time value
     * @param veclen vector length of each time value
     *
     * @return current 2D slice of norms
     */
    public FloatLargeArray getCurrent2DNormSlice(long[] dims, int axis, long slice, int veclen)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }
        return SliceUtils.get2DNormSlice(getCurrentValue(), dims, axis, slice, (long) veclen);
    }

    /**
     * Returns true if the given data array is of the same type as this
     * TimeData, false otherwise.
     *
     * @param data input data array
     *
     * @return true if the given data array is of the same type as this
     *         TimeData, false otherwise
     */
    private boolean isCompatibleWithDataType(LargeArray data)
    {
        Class clazz = data.getClass();

        if (clazz == LogicLargeArray.class &&
             type == DataArrayType.FIELD_DATA_LOGIC) {

            return true;

        } else if (clazz == UnsignedByteLargeArray.class &&
             type == DataArrayType.FIELD_DATA_BYTE) {

            return true;

        } else if (clazz == ShortLargeArray.class &&
             type == DataArrayType.FIELD_DATA_SHORT) {

            return true;

        } else if (clazz == IntLargeArray.class &&
             type == DataArrayType.FIELD_DATA_INT) {

            return true;

        } else if (clazz == LongLargeArray.class &&
             type == DataArrayType.FIELD_DATA_LONG) {

            return true;

        } else if (clazz == FloatLargeArray.class &&
             type == DataArrayType.FIELD_DATA_FLOAT) {

            return true;

        } else if (clazz == DoubleLargeArray.class &&
             type == DataArrayType.FIELD_DATA_DOUBLE) {

            return true;

        } else if (clazz == ComplexFloatLargeArray.class &&
             type == DataArrayType.FIELD_DATA_COMPLEX) {

            return true;

        } else if (clazz == StringLargeArray.class &&
             type == DataArrayType.FIELD_DATA_STRING) {

            return true;

        } else if (clazz == ObjectLargeArray.class &&
             type == DataArrayType.FIELD_DATA_OBJECT) {

            return true;
        }
        return false;
    }
}
