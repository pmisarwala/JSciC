/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.cells;

/**
 * Point cell.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Point extends Cell {

    /**
     * Constructor of a point.
     *
     * @param nsp space dimension
     * @param i node index
     * @param orientation significant if cell is of dim nspace or is a face of a
     * cell of dim nspace
     */
    public Point(int nsp, int i, byte orientation) {
        type = CellType.POINT;
        nspace = nsp;
        vertices = new int[1];
        vertices[0] = i;
        this.orientation = orientation;
    }

    /**
     * Constructor of a point.
     *
     * @param nsp space dimension
     * @param i node index
     */
    public Point(int nsp, int i) {
        this(nsp, i, (byte) 1);
    }

    @Override
    public Cell[] triangulation() {
        Point[] subdiv = {this};
        return subdiv;
    }

    @Override
    public int[][] triangulationVertices() {
        return new int[][]{vertices};
    }

    /**
     * Generates the array of tetrahedra vertices from the array of point
     * vertices.
     *
     * @param vertices array of vertices
     *
     * @return array of tetrahedra vertices
     */
    public static int[][] triangulationVertices(int[] vertices) {
        return new int[][]{vertices};
    }

    @Override
    public Cell[] faces() {
        return null;
    }

    @Override
    public Cell[] faces(int[] nodes, byte orientation) {
        return null;
    }

    @Override
    public Cell[][] subcells() {
        Point[][] tr = new Point[1][1];
        tr[0][0] = this;
        return tr;
    }

    @Override
    public final int[] normalize() {
        return vertices;
    }

    /**
     * A convenience function that reorders array of vertices to increasing
     * order (as long as possible).
     *
     * @param vertices vertices
     *
     * @return always true
     */
    public static boolean normalize(int[] vertices) {
        return true;
    }

    @Override
    public byte compare(int[] v) {
        if (v.length == 1) {
            return 0;
        } else if (v[0] != vertices[0]) {
            return 0;
        } else {
            return 1;
        }
    }

}
