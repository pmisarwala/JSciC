/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.cells;

import pl.edu.icm.jscic.utils.MatrixMath;
import pl.edu.icm.jlargearrays.FloatLargeArray;

/**
 * Tetrahedron cell.
 * @author Krzysztof S. Nowinski,
 * University of Warsaw, ICM
 */
public class Tetra extends Cell
{

    /**
     * Constructor of a tetrahedron.
     * 
     * @param nsp         space dimension
     * @param i0          node index
     * @param i1          node index
     * @param i2          node index
     * @param i3          node index
     * @param orientation significant if cell is of dim nspace or is a face of a cell of dim nspace
     */
    public Tetra(int nsp, int i0, int i1, int i2, int i3, byte orientation)
    {
        type = CellType.TETRA;
        nspace = nsp;
        vertices = new int[4];
        vertices[0] = i0;
        vertices[1] = i1;
        vertices[2] = i2;
        vertices[3] = i3;
        this.orientation = orientation;
        normalize();
    }

    /**
     * Constructor of a tetrahedron.
     * 
     * @param nsp space dimension
     * @param i0  node index
     * @param i1  node index
     * @param i2  node index
     * @param i3  node index
     */
    public Tetra(int nsp, int i0, int i1, int i2, int i3)
    {
        this(nsp, i0, i1, i2, i3, (byte) 1);
    }

    @Override
    public Cell[][] subcells()
    {
        Cell[][] tr = new Cell[4][];
        tr[3] = triangulation();
        tr[2] = faces();
        Segment[] s = {new Segment(nspace, vertices[0], vertices[1]),
                       new Segment(nspace, vertices[0], vertices[2]),
                       new Segment(nspace, vertices[0], vertices[3]),
                       new Segment(nspace, vertices[1], vertices[2]),
                       new Segment(nspace, vertices[1], vertices[3]),
                       new Segment(nspace, vertices[2], vertices[3])};
        tr[1] = s;
        tr[0] = new Point[vertices.length];
        for (int i = 0; i < vertices.length; i++)
            tr[0][i] = new Point(nspace, vertices[i]);
        return tr;
    }

    @Override
    public Cell[] faces()
    {
        return faces(vertices, orientation);
    }

    @Override
    public Cell[] faces(int[] nodes, byte orientation)
    {
        Triangle[] faces = {new Triangle(nspace, nodes[1], nodes[2], nodes[3], orientation),
                            new Triangle(nspace, nodes[0], nodes[2], nodes[3], (byte) (1 - orientation)),
                            new Triangle(nspace, nodes[0], nodes[1], nodes[3], orientation),
                            new Triangle(nspace, nodes[0], nodes[1], nodes[2], (byte) (1 - orientation))};
        return faces;
    }

    @Override
    public Cell[] triangulation()
    {
        Tetra[] subdiv = {this};
        return subdiv;
    }

    @Override
    public int[][] triangulationVertices()
    {
        return new int[][]{vertices};
    }

   /**
     * Generates the array of triangle vertices from the array of point
     * vertices.
     *
     * @param vertices array of vertices
     *
     * @return array of triangle vertices
     */
    public static int[][] triangulationVertices(int[] vertices)
    {
        return new int[][]{vertices};
    }

    @Override
    public final int[] normalize()
    {
        if (!normalize(vertices))
            orientation = (byte) (1 - orientation);
        return vertices;
    }

    /**
     * A convenience function that reorders array of vertices to increasing
     * order.
     *
     * @param vertices vertices
     *
     * @return always true
     */
    public static boolean normalize(int[] vertices)
    {
        int i = 0, t = 0;
        for (int j = 1; j < 4; j++)
            if (vertices[j] < vertices[i])
                i = j;
        switch (i) {
            case 0:
                break;
            case 1:
                t = vertices[0];
                vertices[0] = vertices[1];
                vertices[1] = vertices[2];
                vertices[2] = t;
                break;
            case 2:
                t = vertices[0];
                vertices[0] = vertices[2];
                vertices[2] = vertices[3];
                vertices[3] = t;
                break;
            case 3:
                t = vertices[0];
                vertices[0] = vertices[3];
                vertices[3] = vertices[1];
                vertices[1] = t;
                break;
            default:
                throw new IllegalArgumentException("Invalid index " + i);
        }
        i = 1;
        for (int j = 2; j < 4; j++)
            if (vertices[j] < vertices[i])
                i = j;
        switch (i) {
            case 1:
                break;
            case 2:
                t = vertices[1];
                vertices[1] = vertices[2];
                vertices[2] = vertices[3];
                vertices[3] = t;
                break;
            case 3:
                t = vertices[1];
                vertices[1] = vertices[3];
                vertices[3] = vertices[2];
                vertices[2] = t;
                break;
            default:
                throw new IllegalArgumentException("Invalid index " + i);
        }
        if (vertices[3] < vertices[2]) {
            t = vertices[2];
            vertices[2] = vertices[3];
            vertices[3] = t;
            return false;
        }
        return true;
    }

    @Override
    public byte compare(int[] v)
    {
        if (v.length != 4)
            return 0;
        return compare(new Tetra(this.nspace, v[0], v[1], v[2], v[3]));
    }

    /**
     * Computes barycentric coordinates.
     * 
     * @param p point coordinates
     * @param coords array of coordinates
     * 
     * @return barycentric coordinates of point p in this tetrahedron
     */
    public TetrahedronPosition barycentricCoords(float[] p, FloatLargeArray coords)
    {
        float[][] A = new float[3][3];
        float[] v0 = new float[3];
        float[] b = new float[3];
        long l = 3 * vertices[0];
        for (int i = 0; i < 3; i++)
            v0[i] = coords.getFloat(l + i);
        for (int i = 0; i < 3; i++) {
            b[i] = p[i] - v0[i];
            for (int j = 0; j < 3; j++)
                A[i][j] = coords.getFloat(3 * (long) vertices[j + 1] + i) - v0[i];
        }
        float[] x;
        try {
            x = MatrixMath.lsolve(A, b);
        } catch (IllegalArgumentException ex) {
            return null;
        }
        if (x == null || x[0] < 0 || x[1] < 0 || x[2] < 0 || x[0] + x[1] + x[2] > 1)
            return null;
        
        float x0 = 1 - (x[0] + x[1] + x[2]);
        TetrahedronPosition result = new TetrahedronPosition(vertices, new float[] {x0, x[0], x[1], x[2]}, this);
        return result;
    }
}
