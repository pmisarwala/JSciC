/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic;

import java.awt.image.BufferedImage;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;

/**
 * Interpolation on LargeArrays.
 * 
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class RegularFieldInterpolator
{

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static byte[] getInterpolatedData(UnsignedByteLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            throw new IllegalArgumentException("data == null || dims == null");
        }
        byte[] c;
        int vlen;
        int inexact; // flag of OR'ed values indicating which dimensions are inexact: 1 - u, 2 - v, 4 - w
        long i, j, k, m, n0, n1;
        switch (dims.length) {
            case 3:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1] * (long) dims[2]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]");
                }
                c = new byte[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                if (w < 0) {
                    w = 0;
                }
                if (w > dims[2] - 1) {
                    w = dims[2] - 1;
                }
                inexact = 0;
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                k = (long) w;
                w -= k;
                if (w != 0) {
                    inexact += 4;
                }
                m = (long) vlen * (((long) dims[1] * k + j) * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                n1 = (long) vlen * (long) dims[0] * (long) dims[1];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getByte(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (u * (0xFF & data.getByte(m + l + vlen)) + (1 - u) * (0xFF & data.getByte(m + l)));
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (v * (0xFF & data.getByte(m + l + n0)) + (1 - v) * (0xFF & data.getByte(m + l)));
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (v * (u * (0xFF & data.getByte(m + l + n0 + vlen)) + (1 - u) * (0xFF & data.getByte(m + l + n0))) +
                                (1 - v) * (u * (0xFF & data.getByte(m + l + vlen)) + (1 - u) * (0xFF & data.getByte(m + l))));
                        }
                        break;
                    case 4:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (w * (0xFF & data.getByte(m + l + n1)) + (1 - w) * (0xFF & data.getByte(m + l)));
                        }
                        break;
                    case 5:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (w * (u * (0xFF & data.getByte(m + l + n1 + vlen)) + (1 - u) * (0xFF & data.getByte(m + l + n1))) +
                                (1 - w) * (u * (0xFF & data.getByte(m + l + vlen)) + (1 - u) * (0xFF & data.getByte(m + l))));
                        }
                        break;
                    case 6:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (w * (v * (0xFF & data.getByte(m + l + n1 + n0)) + (1 - v) * (0xFF & data.getByte(m + l + n1))) +
                                (1 - w) * (v * (0xFF & data.getByte(m + l + n0)) + (1 - v) * (0xFF & data.getByte(m + l))));
                        }
                        break;
                    case 7:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (w * (v * (u * (0xFF & data.getByte(m + l + n1 + n0 + vlen)) + (1 - u) * (0xFF & data.getByte(m + l + n1 + n0))) +
                                (1 - v) * (u * (0xFF & data.getByte(m + l + n1 + vlen)) + (1 - u) * (0xFF & data.getByte(m + l + n1)))) +
                                (1 - w) * (v * (u * (0xFF & data.getByte(m + l + n0 + vlen)) + (1 - u) * (0xFF & data.getByte(m + l + n0))) +
                                (1 - v) * (u * (0xFF & data.getByte(m + l + vlen)) + (1 - u) * (0xFF & data.getByte(m + l)))));
                        }
                        break;
                }
                return c;
            case 2:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1]");
                }
                c = new byte[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                inexact = 0;
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                m = (long) vlen * (j * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getByte(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (u * (0xFF & data.getByte(m + l + vlen)) + (1 - u) * (0xFF & data.getByte(m + l)));
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (v * (0xFF & data.getByte(m + l + n0)) + (1 - v) * (0xFF & data.getByte(m + l)));
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (v * (u * (0xFF & data.getByte(m + l + n0 + vlen)) + (1 - u) * (0xFF & data.getByte(m + l + n0))) +
                                (1 - v) * (u * (0xFF & data.getByte(m + l + vlen)) + (1 - u) * (0xFF & data.getByte(m + l))));
                        }
                        break;
                }
                return c;
            case 1:
                vlen = (int) (data.length() / (long) dims[0]);
                if (data.length() != (long) vlen * (long) dims[0]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0]");
                }
                c = new byte[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                inexact = 0;
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                m = vlen * i;
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getByte(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (byte) (u * (0xFF & data.getByte(m + l + vlen)) + (1 - u) * (0xFF & data.getByte(m + l)));
                        }
                        break;
                }
                return c;
        }
        throw new IllegalArgumentException("Cannot compute interpolated data");
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static byte getInterpolatedScalarData(UnsignedByteLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            return 0;
        }

        switch (dims.length) {
            case 3:
                return getInterpolatedScalarData3D(data, dims, u, v, w);
            case 2:
                return getInterpolatedScalarData2D(data, dims, u, v);
            case 1:
                return getInterpolatedScalarData1D(data, dims, u);
        }
        return 0;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. It has to be an array of length 3
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static byte getInterpolatedScalarData3D(UnsignedByteLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null || dims.length != 3) {
            return 0;
        }
        byte c = 0;
        int inexact; // flag of OR'ed values indicating which dimensions are inexact: 1 - u, 2 - v, 4 - w
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1] * (long) dims[2]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (w < 0) {
            w = 0;
        }
        if (w > dims[2] - 1) {
            w = dims[2] - 1;
        }
        inexact = 0;
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        k = (long) w;
        w -= k;
        if (w != 0) {
            inexact += 4;
        }
        m = ((long) dims[1] * k + j) * (long) dims[0] + i;
        n0 = dims[0];
        n1 = (long) dims[0] * (long) dims[1];
        switch (inexact) {
            case 0:
                c = data.getByte(m);
                break;
            case 1:
                c = (byte) (u * (0xFF & data.getByte(m + 1)) + (1 - u) * (0xFF & data.getByte(m)));
                break;
            case 2:
                c = (byte) (v * (0xFF & data.getByte(m + n0)) + (1 - v) * (0xFF & data.getByte(m)));
                break;
            case 3:
                c = (byte) (v * (u * (0xFF & data.getByte(m + n0 + 1)) + (1 - u) * (0xFF & data.getByte(m + n0))) +
                    (1 - v) * (u * (0xFF & data.getByte(m + 1)) + (1 - u) * (0xFF & data.getByte(m))));
                break;
            case 4:
                c = (byte) (w * (0xFF & data.getByte(m + n1)) + (1 - w) * (0xFF & data.getByte(m)));
                break;
            case 5:
                c = (byte) (w * (u * (0xFF & data.getByte(m + n1 + 1)) + (1 - u) * (0xFF & data.getByte(m + n1))) +
                    (1 - w) * (u * (0xFF & data.getByte(m + 1)) + (1 - u) * (0xFF & data.getByte(m))));
                break;
            case 6:
                c = (byte) (w * (v * (0xFF & data.getByte(m + n1 + n0)) + (1 - v) * (0xFF & data.getByte(m + n1))) +
                    (1 - w) * (v * (0xFF & data.getByte(m + n0)) + (1 - v) * (0xFF & data.getByte(m))));
                break;
            case 7:
                c = (byte) (w * (v * (u * (0xFF & data.getByte(m + n1 + n0 + 1)) + (1 - u) * (0xFF & data.getByte(m + n1 + n0))) +
                    (1 - v) * (u * (0xFF & data.getByte(m + n1 + 1)) + (1 - u) * (0xFF & data.getByte(m + n1)))) +
                    (1 - w) * (v * (u * (0xFF & data.getByte(m + n0 + 1)) + (1 - u) * (0xFF & data.getByte(m + n0))) +
                    (1 - v) * (u * (0xFF & data.getByte(m + 1)) + (1 - u) * (0xFF & data.getByte(m)))));
                break;
        }
        return c;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. It has to be an array of length 2
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * 
     * @return interpolated data
     */
    public static byte getInterpolatedScalarData2D(UnsignedByteLargeArray data, int[] dims, float u, float v)
    {
        if (data == null || dims == null || dims.length != 2) {
            return 0;
        }
        byte c = 0;
        int inexact; // flag of OR'ed values indicating which dimensions are inexact: 1 - u, 2 - v (w is unused)
        long i, j, m, n0;
        if (data.length() != (long) dims[0] * (long) dims[1]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        inexact = 0;
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        m = j * (long) dims[0] + i;
        n0 = dims[0];
        switch (inexact) {
            case 0:
                c = data.getByte(m);
                break;
            case 1:
                c = (byte) (u * (0xFF & data.getByte(m + 1)) + (1 - u) * (0xFF & data.getByte(m)));
                break;
            case 2:
                c = (byte) (v * (0xFF & data.getByte(m + n0)) + (1 - v) * (0xFF & data.getByte(m)));
                break;
            case 3:
                c = (byte) (v * (u * (0xFF & data.getByte(m + n0 + 1)) + (1 - u) * (0xFF & data.getByte(m + n0))) +
                    (1 - v) * (u * (0xFF & data.getByte(m + 1)) + (1 - u) * (0xFF & data.getByte(m))));
                break;
        }
        return c;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. It has to be an array of length 1
     * @param u    index corresponding to dims[0]
     * 
     * @return interpolated data
     */
    public static byte getInterpolatedScalarData1D(UnsignedByteLargeArray data, int[] dims, float u)
    {
        if (data == null || dims == null || dims.length != 1) {
            return 0;
        }
        byte c = 0;
        int inexact; // flag 0 or 1 indicating whether dimension u is inexact (1) or exact (0)
        long i;
        if (data.length() != dims[0]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        inexact = 0;
        i = (long) u; //TODO: BUG: u is trimmed instead of being rounded first!
        u -= i;
        if (u != 0) {
            inexact += 1;
        }

        switch (inexact) {
            case 0:
                c = data.getByte(i);
                break;
            case 1:

                c = (byte) (u * (0xFF & data.getByte(i + 1)) + (1 - u) * (0xFF & data.getByte(i)));
                break;
        }
        return c;
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array.
     * 
     * @param bData unsigned byte input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param outData pre-allocated byte array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(UnsignedByteLargeArray bData, int[] dims, float[] p0, float[][] base,
                                                     byte[] outData, int w, int h, boolean useTrilinearInterpolation)
    {
        if (bData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0) {
            throw new IllegalArgumentException("bData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0");
        }

        if (outData.length != w * h) {
            throw new IllegalArgumentException("outData.length != w * h");
        }

        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        if (useTrilinearInterpolation) {
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        outData[c] = getInterpolatedScalarData(bData, dims, tmp[0], tmp[1], tmp[2]);
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        outData[c] = bData.getByte(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x);
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param data input data (scalar or vector component of a 3D field) of type unsiged byte, short, int, float or double
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateFieldToSliceColormappedImage(LargeArray data, int veclen, int[] dims, float[] p0, float[][] base,
                                                               BufferedImage image, int[] colorMapLUT, float low, float up,
                                                               int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        switch (data.getType()) {
            case UNSIGNED_BYTE:
                interpolateFieldToSliceColormappedImage((UnsignedByteLargeArray) data, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
                break;
            case SHORT:
                interpolateFieldToSliceColormappedImage((ShortLargeArray) data, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
                break;
            case INT:
                interpolateFieldToSliceColormappedImage((IntLargeArray) data, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
                break;
            case FLOAT:
                interpolateFieldToSliceColormappedImage((FloatLargeArray) data, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
                break;
            case DOUBLE:
                interpolateFieldToSliceColormappedImage((DoubleLargeArray) data, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
                break;
            default:
                throw new IllegalArgumentException("Unsupported array type");

        }
    }

    /**
     * Interpolates the provided data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param bData unsigned byte input data (scalar or vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateFieldToSliceColormappedImage(UnsignedByteLargeArray bData, int veclen, int[] dims, float[] p0, float[][] base,
                                                               BufferedImage image, int[] colorMapLUT, float low, float up,
                                                               int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (veclen == 1) {
            interpolateScalarFieldToSliceColormappedImage(bData, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        } else {
            interpolateVectorFieldToSliceColormappedImage(bData, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        }
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param bData unsigned byte input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSliceColormappedImage(UnsignedByteLargeArray bData, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (bData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("bData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (bData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]");
        }
        
        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) (((float) (0xFF & getInterpolatedScalarData(bData, dims, tmp[0], tmp[1], tmp[2])) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) (((float) (0xFF & bData.getByte(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x)) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided vector data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param bData unsigned byte input data (vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateVectorFieldToSliceColormappedImage(UnsignedByteLargeArray bData, int veclen, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (bData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("bData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (veclen <= 1) {
            throw new IllegalArgumentException("veclen <= 1");
        }
        
        if (bData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]*(long)veclen) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]*veclen");
        }
        
        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;
        double val, val1;
        byte[] vect;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        vect = getInterpolatedData(bData, dims, tmp[0], tmp[1], tmp[2]);
                        val = 0;
                        for (int k = 0; k < vect.length; k++) {
                            val += (float) (0xFF & vect[k]) * (float) (0xFF & vect[k]);
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        val = 0;
                        for (int k = 0; k < veclen; k++) {
                            val1 = (float) (0xFF & bData.getByte((z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) * veclen + k));
                            val += val1 * val1;
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image with data dependent transparency.
     * 
     * @param bData unsigned byte input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     * @param transparentData data value (unsigned byte) to make fully transparent
     * @param dataTransparency level of other data values transparency (0-255)
     */
    public static void interpolateScalarFieldToSliceColormappedImage(UnsignedByteLargeArray bData, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation,
                                                                     int transparentData, int dataTransparency)
    {
        if (bData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("bData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (bData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]");
        }
        
        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;
        int d;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        d = (int) (0xFF & getInterpolatedScalarData(bData, dims, tmp[0], tmp[1], tmp[2]));
                        c = (int) (((float) d - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);

                        if (d == transparentData) {
                            image.getRaster().setSample(i, j, 3, 0);
                        } else {
                            image.getRaster().setSample(i, j, 3, dataTransparency);
                        }

                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        d = (int) (0xFF & bData.getByte(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x));
                        c = (int) (((float) d - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);

                        if (d == transparentData) {
                            image.getRaster().setSample(i, j, 3, 0);
                        } else {
                            image.getRaster().setSample(i, j, 3, dataTransparency);
                        }

                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static short[] getInterpolatedData(ShortLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            throw new IllegalArgumentException("data == null || dims == null");
        }
        short[] c;
        int vlen;
        int inexact;
        long i, j, k, m, n0, n1;
        switch (dims.length) {
            case 3:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1] * (long) dims[2]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]");
                }
                c = new short[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                if (w < 0) {
                    w = 0;
                }
                if (w > dims[2] - 1) {
                    w = dims[2] - 1;
                }
                inexact = 0;
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                k = (long) w;
                w -= k;
                if (w != 0) {
                    inexact += 4;
                }
                m = (long) vlen * (((long) dims[1] * k + j) * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                n1 = (long) vlen * (long) dims[0] * (long) dims[1];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getShort(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (u * data.getShort(m + l + vlen) + (1 - u) * data.getShort(m + l));
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (v * data.getShort(m + l + n0) + (1 - v) * data.getShort(m + l));
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (v * (u * data.getShort(m + l + n0 + vlen) + (1 - u) * data.getShort(m + l + n0)) +
                                (1 - v) * (u * data.getShort(m + l + vlen) + (1 - u) * data.getShort(m + l)));
                        }
                        break;
                    case 4:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (w * data.getShort(m + l + n1) + (1 - w) * data.getShort(m + l));
                        }
                        break;
                    case 5:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (w * (u * data.getShort(m + l + n1 + vlen) + (1 - u) * data.getShort(m + l + n1)) +
                                (1 - w) * (u * data.getShort(m + l + vlen) + (1 - u) * data.getShort(m + l)));
                        }
                        break;
                    case 6:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (w * (v * data.getShort(m + l + n1 + n0) + (1 - v) * data.getShort(m + l + n1)) +
                                (1 - w) * (v * data.getShort(m + l + n0) + (1 - v) * data.getShort(m + l)));
                        }
                        break;
                    case 7:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (w * (v * (u * data.getShort(m + l + n1 + n0 + vlen) + (1 - u) * data.getShort(m + l + n1 + n0)) +
                                (1 - v) * (u * data.getShort(m + l + n1 + vlen) + (1 - u) * data.getShort(m + l + n1))) +
                                (1 - w) * (v * (u * data.getShort(m + l + n0 + vlen) + (1 - u) * data.getShort(m + l + n0)) +
                                (1 - v) * (u * data.getShort(m + l + vlen) + (1 - u) * data.getShort(m + l))));
                        }
                        break;
                }
                return c;
            case 2:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1]");
                }
                c = new short[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                inexact = 0;
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }

                m = (long) vlen * (j * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getShort(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (u * data.getShort(m + l + vlen) + (1 - u) * data.getShort(m + l));
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (v * data.getShort(m + l + n0) + (1 - v) * data.getShort(m + l));
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (v * (u * data.getShort(m + l + n0 + vlen) + (1 - u) * data.getShort(m + l + n0)) +
                                (1 - v) * (u * data.getShort(m + l + vlen) + (1 - u) * data.getShort(m + l)));
                        }
                        break;
                }
                return c;
            case 1:
                vlen = (int) (data.length() / dims[0]);
                if (data.length() != (long) vlen * (long) dims[0]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0])");
                }
                c = new short[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                inexact = 0;
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                m = (long) vlen * i;
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getShort(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (short) (u * data.getShort(m + l + vlen) + (1 - u) * data.getShort(m + l));
                        }
                        break;
                }
                return c;
        }
        throw new IllegalArgumentException("Cannot compute interpolated data");
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static short getInterpolatedScalarData(ShortLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            return 0;
        }

        switch (dims.length) {
            case 3:
                return getInterpolatedScalarData3D(data, dims, u, v, w);
            case 2:
                return getInterpolatedScalarData2D(data, dims, u, v);
            case 1:
                return getInterpolatedScalarData1D(data, dims, u);
        }
        return 0;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 3
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static short getInterpolatedScalarData3D(ShortLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null || dims.length != 3) {
            return 0;
        }
        short c = 0;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1] * (long) dims[2]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (w < 0) {
            w = 0;
        }
        if (w > dims[2] - 1) {
            w = dims[2] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        k = (long) w;
        w -= k;
        if (w != 0) {
            inexact += 4;
        }

        m = ((long) dims[1] * k + j) * (long) dims[0] + i;
        n0 = dims[0];
        n1 = (long) dims[0] * (long) dims[1];
        switch (inexact) {
            case 0:
                c = data.getShort(m);
                break;
            case 1:
                c = (short) (u * data.getShort(m + 1) + (1 - u) * data.getShort(m));
                break;
            case 2:
                c = (short) (v * data.getShort(m + n0) + (1 - v) * data.getShort(m));
                break;
            case 3:
                c = (short) (v * (u * data.getShort(m + n0 + 1) + (1 - u) * data.getShort(m + n0)) +
                    (1 - v) * (u * data.getShort(m + 1) + (1 - u) * data.getShort(m)));
                break;
            case 4:
                c = (short) (w * data.getShort(m + n1) + (1 - w) * data.getShort(m));
                break;
            case 5:
                c = (short) (w * (u * data.getShort(m + n1 + 1) + (1 - u) * data.getShort(m + n1)) +
                    (1 - w) * (u * data.getShort(m + 1) + (1 - u) * data.getShort(m)));
                break;
            case 6:
                c = (short) (w * (v * data.getShort(m + n1 + n0) + (1 - v) * data.getShort(m + n1)) +
                    (1 - w) * (v * data.getShort(m + n0) + (1 - v) * data.getShort(m)));
                break;
            case 7:
                c = (short) (w * (v * (u * data.getShort(m + n1 + n0 + 1) + (1 - u) * data.getShort(m + n1 + n0)) +
                    (1 - v) * (u * data.getShort(m + n1 + 1) + (1 - u) * data.getShort(m + n1))) +
                    (1 - w) * (v * (u * data.getShort(m + n0 + 1) + (1 - u) * data.getShort(m + n0)) +
                    (1 - v) * (u * data.getShort(m + 1) + (1 - u) * data.getShort(m))));
                break;
        }
        return c;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 2
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * 
     * @return interpolated data
     */
    public static short getInterpolatedScalarData2D(ShortLargeArray data, int[] dims, float u, float v)
    {
        if (data == null || dims == null || dims.length != 2) {
            return 0;
        }
        short c = 0;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }

        m = j * (long) dims[0] + i;
        n0 = dims[0];
        switch (inexact) {
            case 0:
                c = data.getShort(m);
                break;
            case 1:
                c = (short) (u * data.getShort(m + 1) + (1 - u) * data.getShort(m));
                break;
            case 2:
                c = (short) (v * data.getShort(m + n0) + (1 - v) * data.getShort(m));
                break;
            case 3:
                c = (short) (v * (u * data.getShort(m + n0 + 1) + (1 - u) * data.getShort(m + n0)) +
                    (1 - v) * (u * data.getShort(m + 1) + (1 - u) * data.getShort(m)));
                break;
        }
        return c;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 1
     * @param u    index corresponding to dims[0]
     * 
     * @return interpolated data
     */
    public static short getInterpolatedScalarData1D(ShortLargeArray data, int[] dims, float u)
    {
        if (data == null || dims == null || dims.length != 1) {
            return 0;
        }
        short c = 0;
        int inexact = 0;
        long i;
        if (data.length() != dims[0]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        switch (inexact) {
            case 0:
                c = data.getShort(i);
                break;
            case 1:
                c = (short) (u * data.getShort(i + 1) + (1 - u) * data.getShort(i));
                break;
        }
        return c;
    }

    
    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array.
     * 
     * @param sData short input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(ShortLargeArray sData, int[] dims, float[] p0, float[][] base,
                                                     short[] outData, int w, int h, boolean useTrilinearInterpolation)
    {
        interpolateScalarFieldToSlice(sData, dims, p0, base,
                                      (short) 0, (short) 1, 1.0f, outData,
                                      w, h, useTrilinearInterpolation);
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array with additional data rescaling.
     * 
     * @param sData short input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param sMin lower bound of rescaled data range
     * @param sMax upper bound of rescaled data range
     * @param ss rescaling factor (outValue = (inValue - sMin)*ss
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(ShortLargeArray sData, int[] dims, float[] p0, float[][] base,
                                                     short sMin, short sMax, float ss, short[] outData,
                                                     int w, int h, boolean useTrilinearInterpolation)
    {
        if (sData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0) {
            throw new IllegalArgumentException("sData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0");
        }

        if (outData.length != w * h) {
            throw new IllegalArgumentException("outData.length != w * h");
        }

        long x, y, z;
        short v;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        if (useTrilinearInterpolation) {
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        v = getInterpolatedScalarData(sData, dims, tmp[0], tmp[1], tmp[2]);
                        outData[c] = (short) ((v - sMin) * ss);
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        outData[c] = (short) ((sData.getShort(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) - sMin) * ss);
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

   
    /**
     * Interpolates the provided data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param sData short input data (scalar or vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateFieldToSliceColormappedImage(ShortLargeArray sData, int veclen, int[] dims, float[] p0, float[][] base,
                                                               BufferedImage image, int[] colorMapLUT, float low, float up,
                                                               int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (veclen == 1) {
            interpolateScalarFieldToSliceColormappedImage(sData, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        } else {
            interpolateVectorFieldToSliceColormappedImage(sData, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        }
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image with data dependent transparency.
     * 
     * @param sData short input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     * @param transparentData data value (unsigned byte) to make fully transparent
     * @param dataTransparency level of other data values transparency (0-255)
     */
    private static void interpolateScalarFieldToSliceColormappedImage(ShortLargeArray sData, int[] dims, float[] p0, float[][] base,
                                                                      BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                      int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (sData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("sData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }
        
        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (sData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]");
        }

        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) (((float) getInterpolatedScalarData(sData, dims, tmp[0], tmp[1], tmp[2]) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) (((float) sData.getShort(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided vector data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param sData short input data (vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateVectorFieldToSliceColormappedImage(ShortLargeArray sData, int veclen, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (sData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("sData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }
        
        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (veclen <= 1) {
            throw new IllegalArgumentException("veclen <= 1");
        }
        
        if (sData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]*(long)veclen) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]*veclen");
        }

        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;
        double val, val1;
        short[] vect;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        vect = getInterpolatedData(sData, dims, tmp[0], tmp[1], tmp[2]);
                        val = 0;
                        for (int k = 0; k < vect.length; k++) {
                            val += (double) vect[k] * (double) vect[k];
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        val = 0;
                        for (int k = 0; k < veclen; k++) {
                            val1 = (float) sData.getShort((z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) * veclen + k);
                            val += val1 * val1;
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static int[] getInterpolatedData(IntLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            throw new IllegalArgumentException("data == null || dims == null");
        }
        int[] c;
        int vlen;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        switch (dims.length) {
            case 3:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1] * (long) dims[2]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]");
                }
                c = new int[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                if (w < 0) {
                    w = 0;
                }
                if (w > dims[2] - 1) {
                    w = dims[2] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                k = (long) w;
                w -= k;
                if (w != 0) {
                    inexact += 4;
                }
                m = (long) vlen * (((long) dims[1] * k + j) * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                n1 = (long) vlen * (long) dims[0] * (long) dims[1];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getInt(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (u * data.getInt(m + l + vlen) + (1 - u) * data.getInt(m + l));
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (v * data.getInt(m + l + n0) + (1 - v) * data.getInt(m + l));
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (v * (u * data.getInt(m + l + n0 + vlen) + (1 - u) * data.getInt(m + l + n0)) +
                                (1 - v) * (u * data.getInt(m + l + vlen) + (1 - u) * data.getInt(m + l)));
                        }
                        break;
                    case 4:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (w * data.getInt(m + l + n1) + (1 - w) * data.getInt(m + l));
                        }
                        break;
                    case 5:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (w * (u * data.getInt(m + l + n1 + vlen) + (1 - u) * data.getInt(m + l + n1)) +
                                (1 - w) * (u * data.getInt(m + l + vlen) + (1 - u) * data.getInt(m + l)));
                        }
                        break;
                    case 6:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (w * (v * data.getInt(m + l + n1 + n0) + (1 - v) * data.getInt(m + l + n1)) +
                                (1 - w) * (v * data.getInt(m + l + n0) + (1 - v) * data.getInt(m + l)));
                        }
                        break;
                    case 7:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (w * (v * (u * data.getInt(m + l + n1 + n0 + vlen) + (1 - u) * data.getInt(m + l + n1 + n0)) +
                                (1 - v) * (u * data.getInt(m + l + n1 + vlen) + (1 - u) * data.getInt(m + l + n1))) +
                                (1 - w) * (v * (u * data.getInt(m + l + n0 + vlen) + (1 - u) * data.getInt(m + l + n0)) +
                                (1 - v) * (u * data.getInt(m + l + vlen) + (1 - u) * data.getInt(m + l))));
                        }
                        break;
                }
                return c;
            case 2:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1]");
                }
                c = new int[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                m = (long) vlen * (j * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getInt(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (u * data.getInt(m + l + vlen) + (1 - u) * data.getInt(m + l));
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (v * data.getInt(m + l + n0) + (1 - v) * data.getInt(m + l));
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (v * (u * data.getInt(m + l + n0 + vlen) + (1 - u) * data.getInt(m + l + n0)) +
                                (1 - v) * (u * data.getInt(m + l + vlen) + (1 - u) * data.getInt(m + l)));
                        }
                        break;
                }
                return c;
            case 1:
                vlen = (int) (data.length() / dims[0]);
                if (data.length() != (long) vlen * (long) dims[0]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0]");
                }
                c = new int[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                i = (int) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                m = vlen * i;
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getInt(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = (int) (u * data.getInt(m + l + vlen) + (1 - u) * data.getInt(m + l));
                        }
                        break;
                }
                return c;
        }
        throw new IllegalArgumentException("Cannot compute interpolated data");
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static int getInterpolatedScalarData(IntLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            return 0;
        }

        switch (dims.length) {
            case 3:
                return getInterpolatedScalarData3D(data, dims, u, v, w);
            case 2:
                return getInterpolatedScalarData2D(data, dims, u, v);
            case 1:
                return getInterpolatedScalarData1D(data, dims, u);
        }
        return 0;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 3
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static int getInterpolatedScalarData3D(IntLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null || dims.length != 3) {
            return 0;
        }
        int c = 0;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1] * (long) dims[2]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (w < 0) {
            w = 0;
        }
        if (w > dims[2] - 1) {
            w = dims[2] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        k = (long) w;
        w -= k;
        if (w != 0) {
            inexact += 4;
        }
        m = ((long) dims[1] * k + j) * (long) dims[0] + i;
        n0 = dims[0];
        n1 = (long) dims[0] * (long) dims[1];
        switch (inexact) {
            case 0:
                c = data.getInt(m);
                break;
            case 1:
                c = (int) (u * data.getInt(m + 1) + (1 - u) * data.getInt(m));
                break;
            case 2:
                c = (int) (v * data.getInt(m + n0) + (1 - v) * data.getInt(m));
                break;
            case 3:
                c = (int) (v * (u * data.getInt(m + n0 + 1) + (1 - u) * data.getInt(m + n0)) +
                    (1 - v) * (u * data.getInt(m + 1) + (1 - u) * data.getInt(m)));
                break;
            case 4:
                c = (int) (w * data.getInt(m + n1) + (1 - w) * data.getInt(m));
                break;
            case 5:
                c = (int) (w * (u * data.getInt(m + n1 + 1) + (1 - u) * data.getInt(m + n1)) +
                    (1 - w) * (u * data.getInt(m + 1) + (1 - u) * data.getInt(m)));
                break;
            case 6:
                c = (int) (w * (v * data.getInt(m + n1 + n0) + (1 - v) * data.getInt(m + n1)) +
                    (1 - w) * (v * data.getInt(m + n0) + (1 - v) * data.getInt(m)));
                break;
            case 7:
                c = (int) (w * (v * (u * data.getInt(m + n1 + n0 + 1) + (1 - u) * data.getInt(m + n1 + n0)) +
                    (1 - v) * (u * data.getInt(m + n1 + 1) + (1 - u) * data.getInt(m + n1))) +
                    (1 - w) * (v * (u * data.getInt(m + n0 + 1) + (1 - u) * data.getInt(m + n0)) +
                    (1 - v) * (u * data.getInt(m + 1) + (1 - u) * data.getInt(m))));
                break;
        }
        return c;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 2
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * 
     * @return interpolated data
     */
    public static int getInterpolatedScalarData2D(IntLargeArray data, int[] dims, float u, float v)
    {
        if (data == null || dims == null || dims.length != 2) {
            return 0;
        }
        int c = 0;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        m = j * (long) dims[0] + i;
        n0 = dims[0];
        switch (inexact) {
            case 0:
                c = data.getInt(m);
                break;
            case 1:
                c = (int) (u * data.getInt(m + 1) + (1 - u) * data.getInt(m));
                break;
            case 2:
                c = (int) (v * data.getInt(m + n0) + (1 - v) * data.getInt(m));
                break;
            case 3:
                c = (int) (v * (u * data.getInt(m + n0 + 1) + (1 - u) * data.getInt(m + n0)) +
                    (1 - v) * (u * data.getInt(m + 1) + (1 - u) * data.getInt(m)));
                break;
        }
        return c;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 1
     * @param u    index corresponding to dims[0]
     * 
     * @return interpolated data
     */
    public static int getInterpolatedScalarData1D(IntLargeArray data, int[] dims, float u)
    {
        if (data == null || dims == null || dims.length != 1) {
            return 0;
        }
        int c = 0;
        int inexact = 0;
        long i;
        if (data.length() != dims[0]) {
            return 0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        switch (inexact) {
            case 0:
                c = data.getInt(i);
                break;
            case 1:
                c = (int) (u * data.getInt(i + 1) + (1 - u) * data.getInt(i));
                break;
        }
        return c;
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array.
     * 
     * @param iData int input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(IntLargeArray iData, int[] dims, float[] p0, float[][] base,
                                                     int[] outData, int w, int h, boolean useTrilinearInterpolation)
    {
        interpolateScalarFieldToSlice(iData, dims, p0, base,
                                      0, 1, 1.0f, outData,
                                      w, h, useTrilinearInterpolation);
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array with additional data rescaling.
     * 
     * @param iData int input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param iMin lower bound of rescaled data range
     * @param iMax upper bound of rescaled data range
     * @param is rescaling factor (outValue = (inValue - iMin)*is
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(IntLargeArray iData, int[] dims, float[] p0, float[][] base,
                                                     int iMin, int iMax, float is, int[] outData,
                                                     int w, int h, boolean useTrilinearInterpolation)
    {
        if (iData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0) {
            throw new IllegalArgumentException("iData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0");
        }

        if (outData.length != w * h) {
            throw new IllegalArgumentException("outData.length != w * h");
        }

        long x, y, z;
        int v;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        if (useTrilinearInterpolation) {
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        v = getInterpolatedScalarData(iData, dims, tmp[0], tmp[1], tmp[2]);
                        outData[c] = (int) ((v - iMin) * is);
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        outData[c] = (int) ((iData.getInt(z * dims[0] * dims[1] + y * dims[0] + x) - iMin) * is);
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param iData int input data (scalar or vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateFieldToSliceColormappedImage(IntLargeArray iData, int veclen, int[] dims, float[] p0, float[][] base,
                                                               BufferedImage image, int[] colorMapLUT, float low, float up,
                                                               int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (veclen == 1) {
            interpolateScalarFieldToSliceColormappedImage(iData, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        } else {
            interpolateVectorFieldToSliceColormappedImage(iData, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        }
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param iData int input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSliceColormappedImage(IntLargeArray iData, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (iData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("iData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (iData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]");
        }
        
        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) (((float) getInterpolatedScalarData(iData, dims, tmp[0], tmp[1], tmp[2]) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) (((float) iData.getInt(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided vector data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param iData int input data (vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateVectorFieldToSliceColormappedImage(IntLargeArray iData, int veclen, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (iData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("iData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (veclen <= 1) {
            throw new IllegalArgumentException("veclen <= 1");
        }
        
        if (iData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]*(long)veclen) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]*veclen");
        }

        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;
        double val, val1;
        int[] vect;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        vect = getInterpolatedData(iData, dims, tmp[0], tmp[1], tmp[2]);
                        val = 0;
                        for (int k = 0; k < vect.length; k++) {
                            val += (double) vect[k] * (double) vect[k];
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        val = 0;
                        for (int k = 0; k < veclen; k++) {
                            val1 = (float) iData.getInt((z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) * veclen + k);
                            val += val1 * val1;
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static float[] getInterpolatedData(FloatLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            throw new IllegalArgumentException("data == null || dims == null");
        }
        float[] c;
        int vlen;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        switch (dims.length) {
            case 3:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1] * (long) dims[2]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]");
                }
                c = new float[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                if (w < 0) {
                    w = 0;
                }
                if (w > dims[2] - 1) {
                    w = dims[2] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                k = (long) w;
                w -= k;
                if (w != 0) {
                    inexact += 4;
                }
                m = (long) vlen * (((long) dims[1] * k + j) * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                n1 = (long) vlen * (long) dims[0] * (long) dims[1];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getFloat(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = u * data.getFloat(m + l + vlen) + (1 - u) * data.getFloat(m + l);
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * data.getFloat(m + l + n0) + (1 - v) * data.getFloat(m + l);
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * (u * data.getFloat(m + l + n0 + vlen) + (1 - u) * data.getFloat(m + l + n0)) +
                                (1 - v) * (u * data.getFloat(m + l + vlen) + (1 - u) * data.getFloat(m + l));
                        }
                        break;
                    case 4:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * data.getFloat(m + l + n1) + (1 - w) * data.getFloat(m + l);
                        }
                        break;
                    case 5:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * (u * data.getFloat(m + l + n1 + vlen) + (1 - u) * data.getFloat(m + l + n1)) +
                                (1 - w) * (u * data.getFloat(m + l + vlen) + (1 - u) * data.getFloat(m + l));
                        }
                        break;
                    case 6:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * (v * data.getFloat(m + l + n1 + n0) + (1 - v) * data.getFloat(m + l + n1)) +
                                (1 - w) * (v * data.getFloat(m + l + n0) + (1 - v) * data.getFloat(m + l));
                        }
                        break;
                    case 7:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * (v * (u * data.getFloat(m + l + n1 + n0 + vlen) + (1 - u) * data.getFloat(m + l + n1 + n0)) +
                                (1 - v) * (u * data.getFloat(m + l + n1 + vlen) + (1 - u) * data.getFloat(m + l + n1))) +
                                (1 - w) * (v * (u * data.getFloat(m + l + n0 + vlen) + (1 - u) * data.getFloat(m + l + n0)) +
                                (1 - v) * (u * data.getFloat(m + l + vlen) + (1 - u) * data.getFloat(m + l)));
                        }
                        break;
                }
                return c;
            case 2:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1]");
                }
                c = new float[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                m = (long) vlen * (j * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getFloat(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = u * data.getFloat(m + l + vlen) + (1 - u) * data.getFloat(m + l);
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * data.getFloat(m + l + n0) + (1 - v) * data.getFloat(m + l);
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * (u * data.getFloat(m + l + n0 + vlen) + (1 - u) * data.getFloat(m + l + n0)) +
                                (1 - v) * (u * data.getFloat(m + l + vlen) + (1 - u) * data.getFloat(m + l));
                        }
                        break;
                }
                return c;
            case 1:
                vlen = (int) (data.length() / (long) dims[0]);
                if (data.length() != (long) vlen * (long) dims[0]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0]");
                }
                c = new float[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                m = vlen * i;
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getFloat(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = u * data.getFloat(m + l + vlen) + (1 - u) * data.getFloat(m + l);
                        }
                        break;
                }
                return c;
        }
        throw new IllegalArgumentException("cannot compute interpolated data");
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static float getInterpolatedScalarData(FloatLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            return 0.0f;
        }

        switch (dims.length) {
            case 3:
                return getInterpolatedScalarData3D(data, dims, u, v, w);
            case 2:
                return getInterpolatedScalarData2D(data, dims, u, v);
            case 1:
                return getInterpolatedScalarData1D(data, dims, u);
        }
        return 0.0f;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 3
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static float getInterpolatedScalarData3D(FloatLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null || dims.length != 3) {
            return 0.0f;
        }
        float c = 0.0f;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1] * (long) dims[2]) {
            return 0.0f;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (w < 0) {
            w = 0;
        }
        if (w > dims[2] - 1) {
            w = dims[2] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        k = (long) w;
        w -= k;
        if (w != 0) {
            inexact += 4;
        }
        m = ((long) dims[1] * k + j) * (long) dims[0] + i;
        n0 = dims[0];
        n1 = (long) dims[0] * (long) dims[1];
        switch (inexact) {
            case 0:
                c = data.getFloat(m);
                break;
            case 1:
                c = u * data.getFloat(m + 1) + (1 - u) * data.getFloat(m);
                break;
            case 2:
                c = v * data.getFloat(m + n0) + (1 - v) * data.getFloat(m);
                break;
            case 3:
                c = v * (u * data.getFloat(m + n0 + 1) + (1 - u) * data.getFloat(m + n0)) +
                    (1 - v) * (u * data.getFloat(m + 1) + (1 - u) * data.getFloat(m));
                break;
            case 4:
                c = w * data.getFloat(m + n1) + (1 - w) * data.getFloat(m);
                break;
            case 5:
                c = w * (u * data.getFloat(m + n1 + 1) + (1 - u) * data.getFloat(m + n1)) +
                    (1 - w) * (u * data.getFloat(m + 1) + (1 - u) * data.getFloat(m));
                break;
            case 6:
                c = w * (v * data.getFloat(m + n1 + n0) + (1 - v) * data.getFloat(m + n1)) +
                    (1 - w) * (v * data.getFloat(m + n0) + (1 - v) * data.getFloat(m));
                break;
            case 7:
                c = w * (v * (u * data.getFloat(m + n1 + n0 + 1) + (1 - u) * data.getFloat(m + n1 + n0)) +
                    (1 - v) * (u * data.getFloat(m + n1 + 1) + (1 - u) * data.getFloat(m + n1))) +
                    (1 - w) * (v * (u * data.getFloat(m + n0 + 1) + (1 - u) * data.getFloat(m + n0)) +
                    (1 - v) * (u * data.getFloat(m + 1) + (1 - u) * data.getFloat(m)));
                break;
        }
        return c;
    }

      /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 2
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * 
     * @return interpolated data
     */
    public static float getInterpolatedScalarData2D(FloatLargeArray data, int[] dims, float u, float v)
    {
        if (data == null || dims == null || dims.length != 2) {
            return 0.0f;
        }
        float c = 0.0f;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1]) {
            return 0.0f;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        m = j * dims[0] + i;
        n0 = dims[0];
        switch (inexact) {
            case 0:
                c = data.getFloat(m);
                break;
            case 1:
                c = u * data.getFloat(m + 1) + (1 - u) * data.getFloat(m);
                break;
            case 2:
                c = v * data.getFloat(m + n0) + (1 - v) * data.getFloat(m);
                break;
            case 3:
                c = v * (u * data.getFloat(m + n0 + 1) + (1 - u) * data.getFloat(m + n0)) +
                    (1 - v) * (u * data.getFloat(m + 1) + (1 - u) * data.getFloat(m));
                break;
        }
        return c;
    }

      /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 1
     * @param u    index corresponding to dims[0]
     * 
     * @return interpolated data
     */
    public static float getInterpolatedScalarData1D(FloatLargeArray data, int[] dims, float u)
    {
        if (data == null || dims == null || dims.length != 1) {
            return 0.0f;
        }
        float c = 0.0f;
        int inexact = 0;
        long i;
        if (data.length() != dims[0]) {
            return 0.0f;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        switch (inexact) {
            case 0:
                c = data.getFloat(i);
                break;
            case 1:
                c = u * data.getFloat(i + 1) + (1 - u) * data.getFloat(i);
                break;
        }
        return c;
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array.
     * 
     * @param fData float input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(FloatLargeArray fData, int[] dims, float[] p0, float[][] base,
                                                     float[] outData, int w, int h, boolean useTrilinearInterpolation)
    {
        interpolateScalarFieldToSlice(fData, dims, p0, base,
                                      0.0f, 1.0f, 1.0f, outData,
                                      w, h, useTrilinearInterpolation);
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array with additional data rescaling.
     * 
     * @param fData float input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param fMin lower bound of rescaled data range
     * @param fMax upper bound of rescaled data range
     * @param fs rescaling factor (outValue = (inValue - fMin)*fs
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(FloatLargeArray fData, int[] dims, float[] p0, float[][] base,
                                                     float fMin, float fMax, float fs, float[] outData,
                                                     int w, int h, boolean useTrilinearInterpolation)
    {
        if (fData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0) {
            throw new IllegalArgumentException("fData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0");
        }

        if (outData.length != w * h) {
            throw new IllegalArgumentException("outData.length != w * h");
        }

        long x, y, z;
        float v;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        if (useTrilinearInterpolation) {
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        v = getInterpolatedScalarData(fData, dims, tmp[0], tmp[1], tmp[2]);
                        outData[c] = (v - fMin) * fs;
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        outData[c] = (fData.getFloat(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) - fMin) * fs;
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param fData float input data (scalar or vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateFieldToSliceColormappedImage(FloatLargeArray fData, int veclen, int[] dims, float[] p0, float[][] base,
                                                               BufferedImage image, int[] colorMapLUT, float low, float up,
                                                               int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (veclen == 1) {
            interpolateScalarFieldToSliceColormappedImage(fData, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        } else {
            interpolateVectorFieldToSliceColormappedImage(fData, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        }
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param fData float input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSliceColormappedImage(FloatLargeArray fData, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (fData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("fData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (fData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]");
        }
        
        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) ((getInterpolatedScalarData(fData, dims, tmp[0], tmp[1], tmp[2]) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) ((fData.getFloat(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) - (long) low) * (long) cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided vector data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param fData float input data (vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateVectorFieldToSliceColormappedImage(FloatLargeArray fData, int veclen, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (fData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("fData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (veclen <= 1) {
            throw new IllegalArgumentException("veclen <= 1");
        }
        
        if (fData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]*(long)veclen) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]*veclen");
        }

        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;
        float[] vect;
        double val, val1;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        vect = getInterpolatedData(fData, dims, tmp[0], tmp[1], tmp[2]);
                        val = 0;
                        for (int k = 0; k < vect.length; k++) {
                            val += vect[k] * vect[k];
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        val = 0;
                        for (int v = 0; v < veclen; v++) {
                            val1 = fData.getFloat((z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) * (long) veclen + v);
                            val += val1 * val1;
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);

                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static double[] getInterpolatedData(DoubleLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            throw new IllegalArgumentException("data == null || dims == null");
        }
        double[] c;
        int vlen;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        switch (dims.length) {
            case 3:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1] * (long) dims[2]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1] * (long) dims[2]");
                }
                c = new double[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                if (w < 0) {
                    w = 0;
                }
                if (w > dims[2] - 1) {
                    w = dims[2] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                k = (long) w;
                w -= k;
                if (w != 0) {
                    inexact += 4;
                }
                m = (long) vlen * (((long) dims[1] * k + j) * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                n1 = (long) vlen * (long) dims[0] * (long) dims[1];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getDouble(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = u * data.getDouble(m + l + vlen) + (1 - u) * data.getDouble(m + l);
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * data.getDouble(m + l + n0) + (1 - v) * data.getDouble(m + l);
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * (u * data.getDouble(m + l + n0 + vlen) + (1 - u) * data.getDouble(m + l + n0)) +
                                (1 - v) * (u * data.getDouble(m + l + vlen) + (1 - u) * data.getDouble(m + l));
                        }
                        break;
                    case 4:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * data.getDouble(m + l + n1) + (1 - w) * data.getDouble(m + l);
                        }
                        break;
                    case 5:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * (u * data.getDouble(m + l + n1 + vlen) + (1 - u) * data.getDouble(m + l + n1)) +
                                (1 - w) * (u * data.getDouble(m + l + vlen) + (1 - u) * data.getDouble(m + l));
                        }
                        break;
                    case 6:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * (v * data.getDouble(m + l + n1 + n0) + (1 - v) * data.getDouble(m + l + n1)) +
                                (1 - w) * (v * data.getDouble(m + l + n0) + (1 - v) * data.getDouble(m + l));
                        }
                        break;
                    case 7:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = w * (v * (u * data.getDouble(m + l + n1 + n0 + vlen) + (1 - u) * data.getDouble(m + l + n1 + n0)) +
                                (1 - v) * (u * data.getDouble(m + l + n1 + vlen) + (1 - u) * data.getDouble(m + l + n1))) +
                                (1 - w) * (v * (u * data.getDouble(m + l + n0 + vlen) + (1 - u) * data.getDouble(m + l + n0)) +
                                (1 - v) * (u * data.getDouble(m + l + vlen) + (1 - u) * data.getDouble(m + l)));
                        }
                        break;
                }
                return c;
            case 2:
                vlen = (int) (data.length() / ((long) dims[0] * (long) dims[1]));
                if (data.length() != (long) vlen * (long) dims[0] * (long) dims[1]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0] * (long) dims[1]");
                }
                c = new double[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }

                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                if (v < 0) {
                    v = 0;
                }
                if (v > dims[1] - 1) {
                    v = dims[1] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                j = (long) v;
                v -= j;
                if (v != 0) {
                    inexact += 2;
                }
                m = (long) vlen * (j * (long) dims[0] + i);
                n0 = (long) vlen * (long) dims[0];
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getDouble(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = u * data.getDouble(m + l + vlen) + (1 - u) * data.getDouble(m + l);
                        }
                        break;
                    case 2:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * data.getDouble(m + l + n0) + (1 - v) * data.getDouble(m + l);
                        }
                        break;
                    case 3:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = v * (u * data.getDouble(m + l + n0 + vlen) + (1 - u) * data.getDouble(m + l + n0)) +
                                (1 - v) * (u * data.getDouble(m + l + vlen) + (1 - u) * data.getDouble(m + l));
                        }
                        break;
                }
                return c;
            case 1:
                vlen = (int) (data.length() / dims[0]);
                if (data.length() != (long) vlen * (long) dims[0]) {
                    throw new IllegalArgumentException("data.length() != (long) vlen * (long) dims[0]");
                }
                c = new double[vlen];
                if (vlen == 1) {
                    c[0] = getInterpolatedScalarData(data, dims, u, v, w);
                    return c;
                }
                if (u < 0) {
                    u = 0;
                }
                if (u > dims[0] - 1) {
                    u = dims[0] - 1;
                }
                i = (long) u;
                u -= i;
                if (u != 0) {
                    inexact += 1;
                }
                m = vlen * i;
                switch (inexact) {
                    case 0:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = data.getDouble(m + l);
                        }
                        break;
                    case 1:
                        for (int l = 0; l < vlen; l++) {
                            c[l] = u * data.getDouble(m + l + vlen) + (1 - u) * data.getDouble(m + l);
                        }
                        break;
                }
                return c;
        }
        throw new IllegalArgumentException("Cannot compute interpolated data");
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static double getInterpolatedScalarData(DoubleLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null) {
            return 0.0;
        }

        switch (dims.length) {
            case 3:
                return getInterpolatedScalarData3D(data, dims, u, v, w);
            case 2:
                return getInterpolatedScalarData2D(data, dims, u, v);
            case 1:
                return getInterpolatedScalarData1D(data, dims, u);
        }
        return 0.0f;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 3
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * @param w    index corresponding to dims[2]
     * 
     * @return interpolated data
     */
    public static double getInterpolatedScalarData3D(DoubleLargeArray data, int[] dims, float u, float v, float w)
    {
        if (data == null || dims == null || dims.length != 3) {
            return 0.0;
        }
        double c = 0.0;
        int inexact = 0;
        long i, j, k, m, n0, n1;
        if (data.length() != (long) dims[0] * (long) dims[1] * (long) dims[2]) {
            return 0.0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        if (w < 0) {
            w = 0;
        }
        if (w > dims[2] - 1) {
            w = dims[2] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        k = (long) w;
        w -= k;
        if (w != 0) {
            inexact += 4;
        }
        m = ((long) dims[1] * k + j) * (long) dims[0] + i;
        n0 = dims[0];
        n1 = (long) dims[0] * (long) dims[1];
        switch (inexact) {
            case 0:
                c = data.getDouble(m);
                break;
            case 1:
                c = u * data.getDouble(m + 1) + (1 - u) * data.getDouble(m);
                break;
            case 2:
                c = v * data.getDouble(m + n0) + (1 - v) * data.getDouble(m);
                break;
            case 3:
                c = v * (u * data.getDouble(m + n0 + 1) + (1 - u) * data.getDouble(m + n0)) +
                    (1 - v) * (u * data.getDouble(m + 1) + (1 - u) * data.getDouble(m));
                break;
            case 4:
                c = w * data.getDouble(m + n1) + (1 - w) * data.getDouble(m);
                break;
            case 5:
                c = w * (u * data.getDouble(m + n1 + 1) + (1 - u) * data.getDouble(m + n1)) +
                    (1 - w) * (u * data.getDouble(m + 1) + (1 - u) * data.getDouble(m));
                break;
            case 6:
                c = w * (v * data.getDouble(m + n1 + n0) + (1 - v) * data.getDouble(m + n1)) +
                    (1 - w) * (v * data.getDouble(m + n0) + (1 - v) * data.getDouble(m));
                break;
            case 7:
                c = w * (v * (u * data.getDouble(m + n1 + n0 + 1) + (1 - u) * data.getDouble(m + n1 + n0)) +
                    (1 - v) * (u * data.getDouble(m + n1 + 1) + (1 - u) * data.getDouble(m + n1))) +
                    (1 - w) * (v * (u * data.getDouble(m + n0 + 1) + (1 - u) * data.getDouble(m + n0)) +
                    (1 - v) * (u * data.getDouble(m + 1) + (1 - u) * data.getDouble(m)));
                break;
        }
        return c;
    }

    /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 2
     * @param u    index corresponding to dims[0]
     * @param v    index corresponding to dims[1]
     * 
     * @return interpolated data
     */
    public static double getInterpolatedScalarData2D(DoubleLargeArray data, int[] dims, float u, float v)
    {
        if (data == null || dims == null || dims.length != 2) {
            return 0.0f;
        }
        double c = 0.0;
        int inexact = 0;
        long i, j, m, n0;
        if (data.length() != (long) dims[0] * (long) dims[1]) {
            return 0.0f;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        if (v < 0) {
            v = 0;
        }
        if (v > dims[1] - 1) {
            v = dims[1] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        j = (long) v;
        v -= j;
        if (v != 0) {
            inexact += 2;
        }
        m = j * dims[0] + i;
        n0 = dims[0];
        switch (inexact) {
            case 0:
                c = data.getDouble(m);
                break;
            case 1:
                c = u * data.getDouble(m + 1) + (1 - u) * data.getDouble(m);
                break;
            case 2:
                c = v * data.getDouble(m + n0) + (1 - v) * data.getDouble(m);
                break;
            case 3:
                c = v * (u * data.getDouble(m + n0 + 1) + (1 - u) * data.getDouble(m + n0)) +
                    (1 - v) * (u * data.getDouble(m + 1) + (1 - u) * data.getDouble(m));
                break;
        }
        return c;
    }

   /**
     * Returns interpolated data from an array and given indexes.
     * 
     * @param data input array
     * @param dims dimensions of input array. This array must be of length 1
     * @param u    index corresponding to dims[0]
     * 
     * @return interpolated data
     */
    public static double getInterpolatedScalarData1D(DoubleLargeArray data, int[] dims, float u)
    {
        if (data == null || dims == null || dims.length != 1) {
            return 0.0;
        }
        double c = 0.0;
        int inexact = 0;
        long i;
        if (data.length() != dims[0]) {
            return 0.0;
        }
        if (u < 0) {
            u = 0;
        }
        if (u > dims[0] - 1) {
            u = dims[0] - 1;
        }
        i = (long) u;
        u -= i;
        if (u != 0) {
            inexact += 1;
        }
        switch (inexact) {
            case 0:
                c = data.getDouble(i);
                break;
            case 1:
                c = u * data.getDouble(i + 1) + (1 - u) * data.getDouble(i);
                break;
        }
        return c;
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array.
     * 
     * @param dData double input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(DoubleLargeArray dData, int[] dims, float[] p0, float[][] base,
                                                     double[] outData, int w, int h, boolean useTrilinearInterpolation)
    {
        interpolateScalarFieldToSlice(dData, dims, p0, base,
                                      0.0, 1.0, 1.0, outData,
                                      w, h, useTrilinearInterpolation);
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of array with additional data rescaling.
     * 
     * @param dData double input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param dMin lower bound of rescaled data range
     * @param dMax upper bound of rescaled data range
     * @param ds rescaling factor (outValue = (inValue - dMin)*ds
     * @param outData pre-allocated short array for output slice data
     * @param w output slice width
     * @param h output slice height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSlice(DoubleLargeArray dData, int[] dims, float[] p0, float[][] base,
                                                     double dMin, double dMax, double ds, double[] outData,
                                                     int w, int h, boolean useTrilinearInterpolation)
    {
        if (dData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0) {
            throw new IllegalArgumentException("dData == null || dims == null || outData == null || p0 == null || base == null || w <= 0 || h <= 0");
        }

        if (outData.length != w * h) {
            throw new IllegalArgumentException("outData.length != w * h");
        }

        long x, y, z;
        double v;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        if (useTrilinearInterpolation) {
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        v = getInterpolatedScalarData(dData, dims, tmp[0], tmp[1], tmp[2]);
                        outData[c] = (v - dMin) * ds;
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0, c = 0; j < h; j++) {
                for (int i = 0; i < w; i++, c++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        outData[c] = (dData.getDouble(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) - dMin) * ds;
                    } else {
                        outData[c] = 0;
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param dData double input data (scalar or vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateFieldToSliceColormappedImage(DoubleLargeArray dData, int veclen, int[] dims, float[] p0, float[][] base,
                                                               BufferedImage image, int[] colorMapLUT, float low, float up,
                                                               int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (veclen == 1) {
            interpolateScalarFieldToSliceColormappedImage(dData, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        } else {
            interpolateVectorFieldToSliceColormappedImage(dData, veclen, dims, p0, base, image, colorMapLUT, low, up, fillColor, w, h, useTrilinearInterpolation);
        }
    }

    /**
     * Interpolates the provided scalar data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param dData double input data (scalar component of a 3D field)
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateScalarFieldToSliceColormappedImage(DoubleLargeArray dData, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, double low, double up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (dData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("dData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (dims.length != 3) {
            throw new IllegalArgumentException("dims.length != 3");
        }

        if (dData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]");
        }
        
        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        double cs = (double) colorMapSize / (up - low);
        int c;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) ((getInterpolatedScalarData(dData, dims, tmp[0], tmp[1], tmp[2]) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        c = (int) ((dData.getDouble(z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    /**
     * Interpolates the provided vector data of 3D regular field to arbitrary planar slice given by plane base in the form of colormapped image.
     * 
     * @param dData double input data (vector component of a 3D field)
     * @param veclen vector length of a given input data
     * @param dims source field dimensions
     * @param p0 slice plane origin
     * @param base slice plane base vectors
     * @param image pre-allocated output image of size w x h
     * @param colorMapLUT lookup table for colormapping input data
     * @param low lower bound of colormapping range
     * @param up upper bound of colormapping range
     * @param fillColor int value of out-of-bounds color
     * @param w image width
     * @param h image height
     * @param useTrilinearInterpolation if set to true trilinear interpolation is use, otherwise nearest neighbor
     */
    public static void interpolateVectorFieldToSliceColormappedImage(DoubleLargeArray dData, int veclen, int[] dims, float[] p0, float[][] base,
                                                                     BufferedImage image, int[] colorMapLUT, float low, float up,
                                                                     int fillColor, int w, int h, boolean useTrilinearInterpolation)
    {
        if (dData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null) {
            throw new IllegalArgumentException("dData == null || dims == null || image == null || p0 == null || base == null || w <= 0 || h <= 0 || colorMapLUT == null");
        }

        if (image.getWidth() != w || image.getHeight() != h) {
            throw new IllegalArgumentException("image.getWidth() != w || image.getHeight() != h");
        }

        if (veclen <= 1) {
            throw new IllegalArgumentException("veclen <= 1");
        }
        
        if (dData.length() != (long)dims[0]*(long)dims[1]*(long)dims[2]*(long)veclen) {
            throw new IllegalArgumentException("bData.length() != dims[0]*dims[1]*dims[2]*veclen");
        }

        long x, y, z;
        float[] tmp = new float[3];
        tmp[0] = p0[0];
        tmp[1] = p0[1];
        tmp[2] = p0[2];

        int colorMapSize = colorMapLUT.length - 1;
        float cs = (float) colorMapSize / (up - low);
        int c;
        double val, val1;
        double[] vect;

        if (useTrilinearInterpolation) {
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        vect = getInterpolatedData(dData, dims, tmp[0], tmp[1], tmp[2]);
                        val = 0;
                        for (int k = 0; k < vect.length; k++) {
                            val += vect[k] * vect[k];
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        } else {
            tmp[0] += 0.5f;
            tmp[1] += 0.5f;
            tmp[2] += 0.5f;
            for (int j = 0; j < h; j++) {
                for (int i = 0; i < w; i++) {
                    x = (long) tmp[0];
                    y = (long) tmp[1];
                    z = (long) tmp[2];
                    if (x >= 0 && x < dims[0] && y >= 0 && y < dims[1] && z >= 0 && z < dims[2]) {
                        val = 0;
                        for (int k = 0; k < veclen; k++) {
                            val1 = dData.getDouble((z * (long) dims[0] * (long) dims[1] + y * (long) dims[0] + x) * (long) veclen + k);
                            val += val1 * val1;
                        }
                        val = sqrt(val);
                        c = (int) ((val - low) * cs);
                        if (c < 0) {
                            c = 0;
                        }
                        if (c > colorMapSize) {
                            c = colorMapSize;
                        }
                        image.setRGB(i, j, colorMapLUT[c]);
                    } else {
                        image.setRGB(i, j, fillColor);
                    }
                    tmp[0] += base[0][0];
                    tmp[1] += base[0][1];
                    tmp[2] += base[0][2];
                }
                tmp[0] -= w * base[0][0];
                tmp[1] -= w * base[0][1];
                tmp[2] -= w * base[0][2];
                tmp[0] += base[1][0];
                tmp[1] += base[1][1];
                tmp[2] += base[1][2];
            }
        }
    }

    private RegularFieldInterpolator()
    {
    }
}
