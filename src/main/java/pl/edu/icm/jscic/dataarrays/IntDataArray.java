/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jlargearrays.IntLargeArray;

/**
 *
 * DataArray that stores int elements.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class IntDataArray extends DataArray
{

    private static final long serialVersionUID = 4472677135698511008L;

    /**
     * Creates a new instance of IntDataArray.
     *
     * @param	schema	DataArray schema.
     */
    public IntDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_INT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_INT);
    }

    /**
     * Creates a new instance of constant IntDataArray.
     *
     * @param ndata number of data elements in the IntDataArray
     * @param value constant value
     */
    public IntDataArray(long ndata, Integer value)
    {
        super(DataArrayType.FIELD_DATA_INT, ndata, true);
        this.data = new IntLargeArray(ndata, value);
        timeData = new TimeData(DataArrayType.FIELD_DATA_INT);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of IntDataArray.
     *
     * @param ndata  number of data elements in the IntDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public IntDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_INT, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_INT);
    }

    /**
     * Creates a new instance of IntDataArray.
     *
     * @param data   integer array to be included in the generated IntDataArray
     * @param schema DataArray schema.
     */
    public IntDataArray(IntLargeArray data, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_INT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if(schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }

        this.data = data;
        timeData = new TimeData(DataArrayType.FIELD_DATA_INT);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of IntDataArray.
     *
     * @param tData  integer array to be included in the generated IntDataArray
     * @param schema DataArray schema.
     */
    public IntDataArray(TimeData tData, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_INT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_INT) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public IntDataArray cloneShallow()
    {
        IntDataArray clone;
        if (timeData.isEmpty()) {
            clone = new IntDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new IntDataArray(timeData.cloneShallow(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public IntDataArray cloneDeep()
    {
        IntDataArray clone;
        if (timeData.isEmpty()) {
            clone = new IntDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new IntDataArray(timeData.cloneDeep(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public IntLargeArray getRawArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return (IntLargeArray) data;
    }

    @Override
    public IntLargeArray getRawArray(float time)
    {
        return (IntLargeArray) timeData.getValue(time);
    }

    @Override
    public IntLargeArray produceData(float time)
    {
        return (IntLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

}
