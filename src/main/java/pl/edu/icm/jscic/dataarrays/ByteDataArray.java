/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;

/**
 *
 * DataArray that stores byte elements.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class ByteDataArray extends DataArray
{

    private static final long serialVersionUID = -2295856069544827853L;

    /**
     * Creates a new instance of ByteDataArray.
     *
     * @param	schema DataArray schema
     */
    public ByteDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_BYTE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_BYTE);
    }

    /**
     * Creates a new instance of ByteDataArray.
     *
     * @param ndata  number of data elements in the ByteDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public ByteDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_BYTE, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_BYTE);
    }

    /**
     * Creates a new instance of constant ByteDataArray.
     *
     * @param ndata number of data elements in the ByteDataArray
     * @param value constant value
     */
    public ByteDataArray(long ndata, Byte value)
    {
        super(DataArrayType.FIELD_DATA_BYTE, ndata, true);
        this.data = new UnsignedByteLargeArray(ndata, value);
        timeData = new TimeData(DataArrayType.FIELD_DATA_BYTE);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of ByteDataArray.
     *
     * @param data   byte array, this reference is used internally (the array is not cloned)
     * @param schema DataArray schema
     */
    public ByteDataArray(UnsignedByteLargeArray data, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_BYTE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }
        this.data = data;
        timeData = new TimeData(DataArrayType.FIELD_DATA_BYTE);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of ByteDataArray.
     *
     * @param tData  byte array, this reference is used internally (the array is not cloned)
     * @param schema DataArray schema
     */
    public ByteDataArray(TimeData tData, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_BYTE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        if (tData.getType() != DataArrayType.FIELD_DATA_BYTE) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }
        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public ByteDataArray cloneShallow()
    {
        ByteDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ByteDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ByteDataArray(timeData.cloneShallow(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public ByteDataArray cloneDeep()
    {
        ByteDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ByteDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ByteDataArray(timeData.cloneDeep(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public UnsignedByteLargeArray getRawArray(float time)
    {
        return (UnsignedByteLargeArray) timeData.getValue(time);
    }

    @Override
    public UnsignedByteLargeArray produceData(float time)
    {
        return (UnsignedByteLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

    @Override
    public UnsignedByteLargeArray getRawArray()
    {
        if (data == null) {
            setCurrentTime(currentTime);
        }
        return (UnsignedByteLargeArray) data;
    }

}
