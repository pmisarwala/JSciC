/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.dataarrays;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import pl.edu.icm.jscic.TimeData;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.ObjectLargeArray;

/**
 *
 * DataArray that stores DataObjectInterface elements.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 * @param <T> type extending DataObjectInterface
 */
public class ObjectDataArray<T extends DataObjectInterface> extends DataArray
{

    private static final long serialVersionUID = 3920006254051072816L;

    /**
     * Creates a new instance of ObjectDataArray.
     *
     * @param	schema	DataArray schema
     */
    public ObjectDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_OBJECT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_OBJECT);
    }

    /**
     * Creates a new instance of constant ObjectDataArray.
     *
     * @param ndata number of data elements in the ObjectDataArray
     * @param value constant value
     */
    public ObjectDataArray(long ndata, Object value)
    {
        super(DataArrayType.FIELD_DATA_OBJECT, ndata, true);
        if (value instanceof DataObjectInterface) {
            this.data = new ObjectLargeArray(ndata, value);
            timeData = new TimeData(DataArrayType.FIELD_DATA_OBJECT);
            timeData.addValue(data);
            recomputeStatistics();
        }
        else {
            throw new IllegalArgumentException("Constant value " + value + " does not match the ObjectDataArray type");
        }
    }

    /**
     * Creates a new instance of ObjectDataArray.
     *
     * @param ndata  number of data elements in the ObjectDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public ObjectDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_OBJECT, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_OBJECT);
    }

    /**
     * Creates a new instance of ObjectDataArray.
     *
     * @param data   object array to be included in the generated ObjectDataArray
     * @param schema DataArray schema
     */
    public ObjectDataArray(ObjectLargeArray data, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_OBJECT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if(schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }
        this.data = data;
        timeData = new TimeData(DataArrayType.FIELD_DATA_OBJECT);
        timeData.addValue(data);
        recomputeStatistics();
    }

    /**
     * Creates a new instance of ObjectDataArray.
     *
     * @param tData  object array to be included in the generated ObjectDataArray
     * @param schema DataArray schema
     */
    public ObjectDataArray(TimeData tData, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_OBJECT) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_OBJECT) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        timeData = tData;
        setCurrentTime(currentTime);
        recomputeStatistics();
    }

    @Override
    public ObjectDataArray cloneShallow()
    {
        ObjectDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ObjectDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ObjectDataArray(timeData.cloneShallow(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public ObjectDataArray cloneDeep()
    {
        ObjectDataArray clone;
        if (timeData.isEmpty()) {
            clone = new ObjectDataArray(schema.cloneDeep());
            clone.currentTime = currentTime;
        } else {
            clone = new ObjectDataArray(timeData.cloneDeep(), schema.cloneDeep());
            clone.setCurrentTime(currentTime);
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public float[] getFloatElement(long n)
    {
        if (data == null)
            setCurrentTime(currentTime);
        int veclen = schema.getVectorLength();
        float[] out = new float[veclen];
        for (long i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = ((DataObjectInterface) data.get(j)).toFloat();
        }
        return out;
    }
    
    @Override
    public double[] getDoubleElement(long n)
    {
        if (data == null)
            setCurrentTime(currentTime);
        int veclen = schema.getVectorLength();
        double[] out = new double[veclen];
        for (long i = 0, j = n * veclen; i < veclen; i++, j++) {
            out[(int) i] = ((DataObjectInterface) data.get(j)).toFloat();
        }
        return out;
    }

    @Override
    public ObjectLargeArray getRawArray()
    {
        if (data == null)
            setCurrentTime(currentTime);
        return (ObjectLargeArray) data;
    }

    @Override
    public ObjectLargeArray getRawArray(float time)
    {
        return (ObjectLargeArray) timeData.getValue(time);
    }

    @Override
    public ObjectLargeArray produceData(float time)
    {
        return (ObjectLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }
    
     /**
     * Returns the histogram of the current time step.
     *
     * @return the histogram of the current time step.
     */
    @Override
    public long[] getCurrentHistogram() {
        return getHistogram(getPreferredMinValue(), getPreferredMaxValue(), 256, false);
    }

    /**
     * Returns the histogram of the current time step.
     *
     * @param min min value
     * @param max max value
     * @param nbin number of bins
     *
     * @return the histogram of the current time step.
     */
    @Override
    public long[] getCurrentHistogram(final double min, final double max, final int nbin) {
        return getHistogram(min, max, nbin, false);
    }

    /**
     * Returns the histogram of all time steps.
     *
     * @return the histogram of all time steps.
     */
    @Override
    public long[] getHistogram() {
        return getHistogram(getPreferredMinValue(), getPreferredMaxValue(), 256, true);
    }

    /**
     * Returns the histogram of all time steps.
     *
     * @param min min value
     * @param max max value
     * @param nbin number of bins
     *
     * @return the histogram of all time steps.
     */
    @Override
    public long[] getHistogram(final double min, final double max, final int nbin) {
        return getHistogram(min, max, nbin, true);
    }

    private long[] getHistogram(final double min, final double max, final int nbin, boolean computeGlobalHistogram)
    {
        if (nbin < 2) throw new IllegalArgumentException("Number of bins must be greater than 1.");
        long[] valueHistogram = new long[nbin];
        final int vlen = getVectorLength();
        final double binSize;
        if (min == max) {
            binSize = 1;
        } else {
            binSize = (max - min) / (nbin - 1);
        }
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        if (computeGlobalHistogram) {
            ArrayList<LargeArray> vals = timeData.getValues();
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final LargeArray dta = vals.get(step);
                long length = dta.length() / vlen;
                nthreads = (int) min(nthreads, length);
                long k = length / nthreads;
                futures = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = j * k * vlen;
                    final long lastIdx = (j == nthreads - 1) ? length * vlen : firstIdx + k * vlen;
                    futures[j] = ConcurrencyUtils.submit(new Callable<long[]>()
                    {
                        @Override
                        public long[] call() throws Exception
                        {
                            long[] valueHistogram = new long[nbin];
                            if (vlen == 1) {
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    double f = (double) ((DataObjectInterface) dta.get(i)).toFloat();
                                    int bin = (int) ((f - min) / binSize);
                                    if (bin < 0 || bin > nbin - 1) continue;
                                    valueHistogram[bin] += 1;
                                }
                            } else {
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    double f = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        double tmp = (double) ((DataObjectInterface) dta.get(i + j)).toFloat();
                                        f += tmp * tmp;
                                    }
                                    f = sqrt(f);
                                    int bin = (int) ((f - min) / binSize);
                                    if (bin < 0 || bin > nbin - 1) continue;
                                    valueHistogram[bin] += 1;
                                }
                            }
                            return valueHistogram;
                        }
                    });
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        long[] res = (long[]) futures[j].get();
                        for (int i = 0; i < nbin; i++) {
                            valueHistogram[i] += res[i];
                        }
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        } else {
            final LargeArray dta = timeData.getCurrentValue();
            long length = dta.length() / vlen;
            nthreads = (int) min(nthreads, length);
            long k = length / nthreads;
            futures = new Future[nthreads];
            for (int j = 0; j < nthreads; j++) {
                final long firstIdx = j * k * vlen;
                final long lastIdx = (j == nthreads - 1) ? length * vlen : firstIdx + k * vlen;
                futures[j] = ConcurrencyUtils.submit(new Callable<long[]>()
                {
                    @Override
                    public long[] call() throws Exception
                    {
                        long[] valueHistogram = new long[nbin];
                        if (vlen == 1) {
                            for (long i = firstIdx; i < lastIdx; i++) {
                                double f = (double) ((DataObjectInterface) dta.get(i)).toFloat();
                                int bin = (int) ((f - min) / binSize);
                                if (bin < 0 || bin > nbin - 1) continue;
                                valueHistogram[bin] += 1;
                            }
                        } else {
                            for (long i = firstIdx; i < lastIdx; i += vlen) {
                                double f = 0;
                                for (long j = 0; j < vlen; j++) {
                                    double tmp = (double) ((DataObjectInterface) dta.get(i + j)).toFloat();
                                    f += tmp * tmp;
                                }
                                f = sqrt(f);
                                int bin = (int) ((f - min) / binSize);
                                if (bin < 0 || bin > nbin - 1) continue;
                                valueHistogram[bin] += 1;
                            }
                        }
                        return valueHistogram;
                    }
                });
            }
            try {
                for (int j = 0; j < nthreads; j++) {
                    long[] res = (long[]) futures[j].get();
                    for (int i = 0; i < nbin; i++) {
                        valueHistogram[i] += res[i];
                    }
                }
            } catch (InterruptedException | ExecutionException ex) {
                throw new IllegalStateException(ex);
            }
        }

        return valueHistogram;
    }

    @Override
    public final void recomputeStatistics(TimeData timeMask, boolean recomputePreferredMinMax)
    {
        final int vlen = getVectorLength();
        double minv = Double.MAX_VALUE;
        double maxv = -Double.MAX_VALUE;
        double meanv = 0;
        double mean2v = 0;
        long mlength = 0;
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        if (timeMask == null || timeMask.getNSteps() <= 0) {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final LargeArray dta = timeData.getValues().get(step);
                if (vlen == 1) {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    DataObjectInterface tmp = (DataObjectInterface) dta.get(i);
                                    double v = tmp != null ? tmp.toFloat() : 0;
                                    mean += v;
                                    mean2 += v * v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dta.length() / vlen;
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        DataObjectInterface tmp = (DataObjectInterface) dta.get(i + j);
                                        double val = tmp != null ? tmp.toFloat() : 0;
                                        v += val * val;
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        } else {
            for (int step = 0; step < timeData.getNSteps(); step++) {
                final LargeArray dta = timeData.getValues().get(step);
                final LargeArray mask;
                if (timeMask.getNSteps() != timeData.getNSteps()) {
                    mask = timeMask.getValues().get(0);
                } else {
                    mask = timeMask.getValues().get(step);
                }
                if (vlen == 1) {
                    final long length = dta.length();
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i++) {
                                    if (mask.getByte(i) == 0) {
                                        continue;
                                    }
                                    DataObjectInterface tmp = (DataObjectInterface) dta.get(i);
                                    double v = tmp != null ? tmp.toFloat() : 0;
                                    mean += v;
                                    mean2 += v * v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                } else {
                    final long length = dta.length() / vlen;
                    nthreads = (int) min(nthreads, length);
                    long k = length / nthreads;
                    futures = new Future[nthreads];
                    for (int j = 0; j < nthreads; j++) {
                        final long firstIdx = j * k;
                        final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
                        futures[j] = ConcurrencyUtils.submit(new Callable<double[]>()
                        {
                            @Override
                            public double[] call() throws Exception
                            {
                                double min = Double.MAX_VALUE;
                                double max = -Double.MAX_VALUE;
                                double mean = 0;
                                double mean2 = 0;
                                long mlength = 0;
                                for (long i = firstIdx; i < lastIdx; i += vlen) {
                                    if (mask.getByte(i) == 0) {
                                        continue;
                                    }
                                    double v = 0;
                                    for (long j = 0; j < vlen; j++) {
                                        DataObjectInterface tmp = (DataObjectInterface) dta.get(i + j);
                                        double val = tmp != null ? tmp.toFloat() : 0;
                                        v += val * val;
                                    }
                                    mean2 += v;
                                    v = sqrt(v);
                                    mean += v;
                                    mlength++;
                                    if (v < min) {
                                        min = v;
                                    }
                                    if (v > max) {
                                        max = v;
                                    }
                                }
                                return new double[]{min, max, mean, mean2, (double) mlength};
                            }
                        });
                    }
                }
                try {
                    for (int j = 0; j < nthreads; j++) {
                        double[] res = (double[]) futures[j].get();
                        if (res[0] < minv) {
                            minv = res[0];
                        }
                        if (res[1] > maxv) {
                            maxv = res[1];
                        }
                        meanv += res[2];
                        mean2v += res[3];
                        mlength += res[4];
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }
        meanv /= (double) mlength;
        mean2v /= (double) mlength;
        setStatistics(minv, maxv, meanv, mean2v, recomputePreferredMinMax);

    }
}
