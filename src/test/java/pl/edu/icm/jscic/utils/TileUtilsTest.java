/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic.utils;

import org.junit.*;
import static org.junit.Assert.*;
import pl.edu.icm.jlargearrays.IntLargeArray;

/**
 *
 * @author know
 */
public class TileUtilsTest
{

    public TileUtilsTest()
    {
    }

    private static final int[] DIMS3 = {8, 8, 8};
    private static final int[] DIMS3D = {4, 4, 4};
    private static final int[] DIMS2 = {8, 8};
    private static final int[] DIMS2D = {4,4};
    private int[][][][] tile3    = new int[2][2][2][125];
    private int[][][][][] tile3c = new int[2][2][2][3][125];
    private int[][][][] tile3v   = new int[2][2][2][375];
    private int[][][] tile2      = new int[2][2][25];
    private int[][][] tile2v     = new int[2][2][50];
    private static final int[][] TILEDEFS = {{-1, 3},{4, 8}};
    private static final int[][] TILEDEFSD = {{-1, 3, 2},{4, 8, 2}};
    
    
    @Before
    public void prepareData()
    {
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 2; k++)
                    for (int l = 0, p = 0; l < 5; l++)
                        for (int m = 0; m < 5; m++)
                            for (int n = 0; n < 5; n++, p++) {
                                tile3[i][j][k][p] = ((5 * i + l) * 10 + 5 * j + m) * 10 + 5 * k + n;
                                tile3c[i][j][k][0][p] = tile3v[i][j][k][3 * p] = 5 * k + n;
                                tile3c[i][j][k][1][p] = tile3v[i][j][k][3 * p + 1] = 5 * j + m;
                                tile3c[i][j][k][2][p] = tile3v[i][j][k][3 * p + 2] = 5 * i + l;
                            }
        for (int j = 0; j < 2; j++)
            for (int k = 0; k < 2; k++)
                for (int m = 0, p = 0; m < 5; m++)
                    for (int n = 0; n < 5; n++, p++) {
                        tile2 [j][k][p]         = (5 * j + m) * 10 + 5 * k + n;
                        tile2v[j][k][2 * p]     =  5 * k + n;
                        tile2v[j][k][2 * p + 1] =  5 * j + m;
                        }   
    }
    
    /**
     * Test of putTile method, of class TileUtils.
     */
    @Test
    public void testPutTile_5args() 
    {
        IntLargeArray target = new IntLargeArray(512);
        int[][] tile;
        
        for (int i = 0; i < 2; i++) 
            for (int j = 0; j < 2; j++) 
                for (int k = 0; k < 2; k++) {
                    tile = new int[][] {TILEDEFS[k], TILEDEFS[j], TILEDEFS[i]};
                    TileUtils.putTile(tile, DIMS3, target, new IntLargeArray(tile3[i][j][k]), 1);
                }
        boolean failed3s = false;
        for (int i = 0; i < DIMS3[2]; i++) 
            for (int j = 0; j < DIMS3[1]; j++) 
                for (int k = 0; k < DIMS3[0]; k++) 
                    if (target.get(64 * i + 8 * j + k) != 100 * (i + 1) + 10 * (j + 1) + k + 1)
                        failed3s = true;
        
        target = new IntLargeArray(3 * 512);
        for (int i = 0; i < 2; i++) 
            for (int j = 0; j < 2; j++) 
                for (int k = 0; k < 2; k++) {
                    tile = new int[][] {TILEDEFS[k], TILEDEFS[j], TILEDEFS[i]};
                    TileUtils.putTile(tile, DIMS3, target, new IntLargeArray(tile3v[i][j][k]), 3);
                }
        boolean failed3v = false;
        for (int i = 0, l = 0; i < DIMS3[2]; i++) 
            for (int j = 0; j < DIMS3[1]; j++) 
                for (int k = 0; k < DIMS3[0]; k++, l++) 
                    if (target.get(3 * l)     != k + 1 ||
                        target.get(3 * l + 1) != j + 1 ||
                        target.get(3 * l + 2) != i + 1)
                        failed3v = true;
       
        target = new IntLargeArray(64);
        for (int i = 0; i < 2; i++) 
            for (int j = 0; j < 2; j++) 
                for (int k = 0; k < 2; k++) {
                    tile = new int[][] {TILEDEFSD[k], TILEDEFSD[j], TILEDEFSD[i]};
                    TileUtils.putTile(tile, DIMS3D, target, new IntLargeArray(tile3[i][j][k]), 1);
                }
        boolean failed3sd = false;
        for (int i = 0, l = 0; i < DIMS3D[2]; i++) 
            for (int j = 0; j < DIMS3D[1]; j++) 
                for (int k = 0; k < DIMS3D[0]; k++, l++)
                    if (target.get(l) != 100 * (TILEDEFSD[0][2] * i + 1) + 10 * (TILEDEFSD[0][2] * j + 1) + TILEDEFSD[0][2] * k + 1)
                        failed3sd = true;
        
        target = new IntLargeArray(64);
        for (int j = 0; j < 2; j++) 
            for (int k = 0; k < 2; k++) {
                tile = new int[][] {TILEDEFS[k], TILEDEFS[j]};
                TileUtils.putTile(tile, DIMS2, target, new IntLargeArray(tile2[j][k]), 1);
            }
        boolean failed2s = false;
        for (int j = 0, l = 0; j < DIMS2[1]; j++) 
            for (int k = 0; k < DIMS2[0]; k++, l++) 
                if (target.get(l) != 10 * (j + 1) + k + 1)
                    failed2s = true;
        
        target = new IntLargeArray(16);
        for (int j = 0; j < 2; j++) 
            for (int k = 0; k < 2; k++) {
                tile = new int[][] {TILEDEFSD[k], TILEDEFSD[j]};
                TileUtils.putTile(tile, DIMS2D, target, new IntLargeArray(tile2[j][k]), 1);
            }
        boolean failed2sd = false;
        for (int j = 0, l = 0; j < DIMS2D[1]; j++) 
            for (int k = 0; k < DIMS2D[0]; k++, l++) 
                if (target.get(l) != 10 * (TILEDEFSD[0][2] * j + 1) + TILEDEFSD[0][2] * k + 1)
                    failed2sd = true;
        
        String fail = (failed2s ? "2d scalar  " : " ") + 
                      (failed2sd ? "2d scalar downsize  " : " ") + 
                      (failed3s ? "3d scalar  " : " ") + 
                      (failed3sd ? "3d scalar downsize fail " : " ") + 
                      (failed3v ? "3d vector fail " : " ");
        if (failed2sd || failed2s || failed3sd || failed3v || failed3s)
            fail("Failed tile copying tests:"+fail);
    }


    /**
     * Test of putTile method, of class TileUtils.
     */
    @Test
    public void testPutTile_6args()
    {
        IntLargeArray target = new IntLargeArray(3 * 512);
        for (int i = 0; i < 2; i++) 
            for (int j = 0; j < 2; j++) 
                for (int k = 0; k < 2; k++) {
                    int[][] tile = new int[][]{TILEDEFS[k], TILEDEFS[j], TILEDEFS[i]};
                    for (int l = 0; l < 3; l++)
                        TileUtils.putTile(tile, DIMS3, target, new IntLargeArray(tile3c[i][j][k][l]), 3, l);
                }
        boolean failed3v = false;
        for (int i = 0, l = 0; i < DIMS3[2]; i++) 
            for (int j = 0; j < DIMS3[1]; j++) 
                for (int k = 0; k < DIMS3[0]; k++, l++) 
                    if (target.get(3 * l)     != k + 1 ||
                        target.get(3 * l + 1) != j + 1 ||
                        target.get(3 * l + 2) != i + 1)
                        failed3v = true;
               
        if (failed3v )
            fail("Failed component copying test");
        
    }
}
